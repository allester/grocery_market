<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    // Point to table in database
    protected $table = 'vendors';

    public function market() {
        return $this->belongsTo('App\Market', 'market_id', 'id');
    }

}
