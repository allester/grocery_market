<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    // Point to table in database
    protected $table = 'order_product';
}
