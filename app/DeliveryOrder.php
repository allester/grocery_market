<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrder extends Model
{
    // Point to the table in database
    protected $table = 'delivery_order';
}
