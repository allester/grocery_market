<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {

            if ($guard == "customer") {
                // User was authenticated with customer guard
                return redirect()->route('welcome');
            } else {
                // default guard
                return redirect()->route('dashboard');
            }
        }

        return $next($request);
    }
}
