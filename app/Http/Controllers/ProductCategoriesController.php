<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;

class ProductCategoriesController extends Controller
{

    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retrieve all categories from database
        $product_categories = ProductCategory::all();
        // print($product_categories);

        return view('product_categories/index')->with('product_categories', $product_categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product_categories/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_category' => 'required'
        ]);
        // print($request);

        // Get the user ID
        $user_id = auth()->user()->id;

        // Generate obfuscator
        $obfuscator = Str::random(5);
        // print($obfuscator);exit;

        if ($request->hasFile('picture')) {

            // Get file name with extension
            $fileNameWithExt = $request->file('picture')->getClientOriginalName();

            // Get file name only
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get the file extension only
            $extension = $request->file('picture')->getClientOriginalExtension();

            // Create file name to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            // Upload image
            $path = $request->file('picture')->storeAs('public/uploads/categories', $fileNameToStore);

        } else {

            $fileNameToStore = 'nofile.png';

        }

        // id 	category 	category_image 	user_id 	edited_by 	created_at 	updated_at

        $product_category = new ProductCategory();

        $product_category->category = ucfirst($request->input('product_category'));
        $product_category->category_image = $fileNameToStore;
        $product_category->user_id = $user_id;
        $product_category->obfuscator = $obfuscator;

        // Save record to database
        if ($product_category->save()) {
            return redirect('product_categories')->with('success', 'Category details have been saved');
        } else {
            return redirect('product_categories')->with('error', 'Category details have not been saved');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // print($id);

        $obfuscator = $id;
        // print($obfuscator);exit;

        // Get category from database
        $product_category = ProductCategory::where('obfuscator',$obfuscator)->first();
        // print($product_category->category);exit;

        return view('product_categories/show')->with('product_category', $product_category);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // print($id);

        $obfuscator = $id;

        // Retrieve category from database
        $product_category = ProductCategory::where('obfuscator', $obfuscator)->first();
        // print($product_category);

        return view('product_categories/edit')->with('product_category', $product_category);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_category' => 'required'
        ]);
        // print($request);

        $category_id = $id;

        // Get the user's ID
        $edited_by = auth()->user()->id;

        if ($request->hasFile('picture')) {

            // get file name with extension
            $fileNameWithExt = $request->file('picture')->getClientOriginalName();

            // Get file name only
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get the file extension only
            $extension = $request->file('picture')->getClientOriginalExtension();

            // Create file name to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            // Upload image
            $path = $request->file('picture')->storeAs('public/uploads/categories', $fileNameToStore);

            // {{-- id 	category 	category_image 	user_id 	edited_by 	created_at 	updated_at --}}

            $product_category = ProductCategory::find($category_id);

            // Delete current picture from database/ storage
            $picture = $product_category->category_image;

            if (Storage::delete('public/uploads/categories/'.$picture)) {

                $product_category->category = ucfirst($request->input('product_category'));
                $product_category->category_image = $fileNameToStore;
                $product_category->edited_by = $edited_by;

                if ($product_category->save()) {
                    return redirect('product_categories')->with('success', 'Category details have been updated');
                } else {
                    return redirect('product_categories')->with('error', 'Category details have not been updated');
                }

            }

        } else {

            $fileNameToStore = 'nofile.pdf';

            $product_category = ProductCategory::find($category_id);

            $product_category->category = ucfirst($request->input('product_category'));
            $product_category->edited_by = $edited_by;

            if ($product_category->save()) {
                return redirect('product_categories')->with('success', 'Category details have been updated');
            } else {
                return redirect('product_categories')->with('error', 'Category details have not been updated');
            }

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // print($id);

        $obfuscator = $id;

        // Retrieve record from database
        $product_category = ProductCategory::where('obfuscator', $obfuscator)->first();
        // print($product_category);

        if ($product_category) {
            if ($product_category->delete()) {
                return redirect('product_categories')->with('success', 'Category details have been deleted');
            } else {
                return redirect('product_categories')->with('error', 'Category details have not been deleted');
            }
        }
    }
}
