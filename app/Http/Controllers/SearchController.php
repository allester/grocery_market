<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;
use App\Brand;
use App\Customer;
use App\CustomerAddress;
use App\DeliveryOrder;
use App\DeliveryTeam;
use App\Order;
use App\OrderDelivery;
use App\OrderProduct;
use App\Product;
use App\ProductCategory;
use App\ProductSubCategory;
use App\Region;
use App\StockTracking;
use DB;

class SearchController extends Controller
{

    public function search_products(Request $request){

        // print($request);

        $query = $request->input('search');
        // print($query);exit;

        if (!$query) {
            return redirect()->back();
        }

        // $categories = ProductCategory::where('category', 'LIKE', "%{$query}%")->get();
        // print($categories);

        $sub_category = ProductSubCategory::where('sub_category', 'LIKE', "%{$query}%")->first();

        $products = '';

        if ($sub_category) {
            // foreach ($sub_categories as $sub_category) {
                // print($sub_category->id);
                $sub_category_id = $sub_category->id;
                $products = Product::where('sub_category_id', $sub_category_id)->get();
            // }
            // print($products);
        } else {
            $products = Product::where('product_name', 'LIKE', "%{$query}%")->orWhere('description', 'LIKE', "%{$query}%")->get();
            // print($products);//exit;
        }

        $result_count = count($products);

        return view('search.store_front_results')->with(['products'=> $products, 'result_count' => $result_count, 'query' => $query]);

    }

    public function autocomplete(Request $request){

        $query = $request->input('query');

        $categories = ProductCategory::select("category")->where("category", "LIKE", "%{$query}%")->get();

        $sub_categories = ProductSubCategory::select("sub_category")->where("sub_category", "LIKE", "%{$query}%")->get();

        $products = Product::select("product_name")->where("product_name", "LIKE", "%{$query}%")->get();

        $results_array = array(
            'categories' => $categories,
            'sub_categories' => $sub_categories,
            'products' => $products
        );

        return response()->json($results_array);

    }

}
