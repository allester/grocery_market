<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory;
use App\Brand;
use App\Market;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        // print($request);exit;
        // print($id);
        $market_obfuscator = $request->market;

        $market = Market::where('obfuscator', $market_obfuscator)->first();
        // print($market);exit;

        $obfuscator = $id;

        // Retrieve details from database
        $item = Product::where('obfuscator', $obfuscator)->first();
        // print($item);

        $category_id = $item->category_id;

        // Other products in the same category
        $other_products = Product::where('category_id', $category_id)->inRandomOrder()->take(3)->get();

        // Get all Categories and brands
        $product_categories = ProductCategory::with('sub_categories')->get();
        // print($product_categories);//exit;

        $brands = Brand::all();
        // print($brands);exit;

        return view('items/show')->with(['item' => $item, 'product_categories' => $product_categories, 'brands' => $brands, 'other_products' => $other_products, 'market' => $market]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
