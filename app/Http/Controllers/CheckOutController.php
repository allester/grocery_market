<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Customer;
use App\CustomerAddress;
use App\Region;
use App\Brand;
use App\Market;
use Cart;

class CheckOutController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // print($request);exit;
        if (false == Auth::guard('customer')->check()) {
            return redirect()->route('customer.login');
        } else {

            $cart_items = '';

            if (count(Cart::content()) > 0) {
                $cart_items = Cart::content();
            }
            // print($cart_items);exit;

            // print($cart_items);

            $customer_id = Auth::guard('customer')->user()->id;
            // print($user_id);

            // Retrieve customer's details
            $customer = Customer::find($customer_id);
            // print($customer);

            // Retrieve customer's billing address
            $customer_address = CustomerAddress::where('customer_id', $customer_id)->first();
            // print($customer_address);

            if ($customer_address == NULL || $customer_address == '') {
                return redirect()->intended(route('customer.profile'))->with('error', 'Please add your billing address before checkout');
            } else {


                // Retrieve regions
                $regions = Region::all();
                // print($regions);

                // Delivery fee
                $delivery_fee = 5000;

                $cart_total = str_replace(',', '', Cart::priceTotal(0));
                // print($cart_total);exit;

                $cart_total = $cart_total;

                $grand_total = $cart_total + $delivery_fee;
                // print(number_format($grand_total));exit;

                $brands = Brand::all();

                // Get markets
                $markets = Market::all();
                // print($markets);exit;


                return view('checkout/index')->with(['regions' => $regions, 'customer' => $customer, 'customer_address' => $customer_address, 'cart_items' => $cart_items, 'delivery_fee' => $delivery_fee, 'grand_total' => $grand_total, 'brands' => $brands, 'markets' => $markets]);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
