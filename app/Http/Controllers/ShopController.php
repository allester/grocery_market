<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory;
use App\ProductSubCategory;
use App\Brand;

use Auth;

use Illuminate\Support\Str;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * ICT Solutions
     *
     */

    public function ict_solutions()
    {

        // Get category
        $category = ProductCategory::where('category', 'ICT')->first();
        // print($category);

        $category_id = $category->id;

        // Get all brands under ICT
        $brands = Brand::where('category_id', $category_id)->get();

        // Get all sub categories under ICT
        $sub_categories = ProductSubCategory::where('category_id', $category_id)->get();
        // print($sub_categories);exit;

        $five_sub_categories = ProductSubCategory::where('category_id', $category_id)->inRandomOrder()->take(5)->get();

        // Get all ICT products
        $products = Product::where('category_id', $category_id)->paginate(10);
        // print($products);

        return view('shop/ict')->with(['category' => $category, 'products' => $products, 'brands' => $brands, 'sub_categories' => $sub_categories, 'five_sub_categories' => $five_sub_categories]);
    }

    public function power_solutions()
    {
        // $categories = ProductCategory::all();
        // print($categories);
        // exit;

        // Get category
        $category = ProductCategory::where('category', 'Power Solutions')->first();
        // print($category);
        // exit;

        $category_id = $category->id;

        // Get all brands under Power Solutions
        $brands = Brand::where('category_id', $category_id)->get();

        // Get all sub categories under ICT
        $sub_categories = ProductSubCategory::where('category_id', $category_id)->get();

        $five_sub_categories = ProductSubCategory::where('category_id', $category_id)->inRandomOrder()->take(5)->get();

        // Get all Power Solutions products
        $products = Product::where('category_id', $category_id)->paginate(10);

        return view('shop/power_solutions')->with(['category' => $category, 'products' => $products, 'brands' => $brands, 'sub_categories' => $sub_categories, 'five_sub_categories' => $five_sub_categories]);
    }

    public function security_solutions()
    {

        // Get category
        $category = ProductCategory::where('category', 'Security Solutions')->first();

        $category_id = $category->id;

        // Get all brands under Security Solutions
        $brands = Brand::where('category_id', $category_id)->get();

        // Get all sub categories under Security Solutions
        $sub_categories = ProductSubCategory::where('category_id', $category_id)->get();

        $five_sub_categories = ProductSubCategory::where('category_id', $category_id)->inRandomOrder()->take(5)->get();

        // Get all Security Solutions products
        $products = Product::where('category_id', $category_id)->paginate(10);

        return view('shop/security_solutions')->with(['category' => $category, 'products' => $products, 'brands' => $brands, 'sub_categories' => $sub_categories, 'five_sub_categories' => $five_sub_categories]);
    }

    public function ict_products(Request $request, $id)
    {
        // print($request);
        // print($id);
        $sub_category_obfuscator = $id;

        $sub_category = ProductSubCategory::where('obfuscator', $sub_category_obfuscator)->first();
        // print($sub_category);

        $sub_category_id = $sub_category->id;

        $category_id = $sub_category->category_id;

        // Get all brands under this category
        $brands = Brand::where('category_id', $category_id)->get();

        // Get category
        $category = ProductCategory::find($category_id);
        // print($category);

        // Get all sub categories under ICT products
        $sub_categories = ProductSubCategory::where('category_id', $category_id)->get();

        $five_sub_categories = ProductSubCategory::where('category_id', $category_id)->inRandomOrder()->take(5)->get();

        // Get all products under selected sub category
        $products = Product::where('sub_category_id', $sub_category_id)->paginate(10);

        return view('shop/sub_category/ict')->with(['category' => $category, 'products' => $products, 'brands' => $brands, 'sub_categories' => $sub_categories, 'five_sub_categories' => $five_sub_categories, 'sub_category' => $sub_category]);
    }

    public function power_products(Request $request, $id)
    {

        $sub_category_obfuscator = $id;

        $sub_category = ProductSubCategory::where('obfuscator', $sub_category_obfuscator)->first();
        // print($sub_category);

        $sub_category_id = $sub_category->id;

        $category_id = $sub_category->category_id;

        // Get all brands under this category
        $brands = Brand::where('category_id', $category_id)->get();

        // Get category
        $category = ProductCategory::find($category_id);
        // print($category);

        // Get all sub categories under Power Products
        $sub_categories = ProductSubCategory::where('category_id', $category_id)->get();

        $five_sub_categories = ProductSubCategory::where('category_id', $category_id)->inRandomOrder()->take(5)->get();

        // Get all products under selected sub category
        $products = Product::where('sub_category_id', $sub_category_id)->paginate(10);

        return view('shop/sub_category/power_solutions')->with(['category' => $category, 'products' => $products, 'brands' => $brands, 'sub_categories' => $sub_categories, 'five_sub_categories' => $five_sub_categories, 'sub_category' => $sub_category]);
    }

    public function security_products(Request $request, $id)
    {

        $sub_category_obfuscator = $id;

        $sub_category = ProductSubCategory::where('obfuscator', $sub_category_obfuscator)->first();
        // print($sub_category);

        $sub_category_id = $sub_category->id;

        $category_id = $sub_category->category_id;

        // Get all brands under this category
        $brands = Brand::where('category_id', $category_id)->get();

        // Get category
        $category = ProductCategory::find($category_id);
        // print($category);

        // Get all sub categories under Security Products
        $sub_categories = ProductSubCategory::where('category_id', $category_id)->get();

        $five_sub_categories = ProductSubCategory::where('category_id', $category_id)->inRandomOrder()->take(5)->get();

        // Get all products under selected sub category
        $products = Product::where('sub_category_id', $sub_category_id)->paginate(10);

        return view('shop/sub_category/security_solutions')->with(['category' => $category, 'products' => $products, 'brands' => $brands, 'sub_categories' => $sub_categories, 'five_sub_categories' => $five_sub_categories, 'sub_category' => $sub_category]);
    }
}
