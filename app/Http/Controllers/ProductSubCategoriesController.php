<?php

namespace App\Http\Controllers;

use App\ProductCategory;
use Illuminate\Http\Request;
use App\ProductSubCategory;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;

class ProductSubCategoriesController extends Controller
{

    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retrieve all sub categories from database
        $sub_categories = ProductSubCategory::paginate(10);
        // print($sub_categories);

        return view('sub_categories/index')->with('sub_categories', $sub_categories);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Retrieve all categories from database
        $product_categories = ProductCategory::all();
        // print($product_categories);

        return view('sub_categories/create')->with('product_categories', $product_categories);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category' => 'required',
            'sub_category' => 'required'
        ]);
        // print($request->category);

        // Get the user ID
        $user_id = auth()->user()->id;

        // Generate obfuscator
        $obfuscator = Str::random(5);
        // print($obfuscator);exit;

        if ($request->hasFile('picture')) {

            // Get file name with extension
            $fileNameWithExt = $request->file('picture')->getClientOriginalName();

            // Get file name only
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get the file extension only
            $extension = $request->file('picture')->getClientOriginalExtension();

            // Create file name to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            // Upload image
            $path = $request->file('picture')->storeAs('public/uploads/sub_categories', $fileNameToStore);

        } else {

            $fileNameToStore = 'nofile.png';

        }

        // id 	sub_category 	sub_category_image 	obfuscator 	category_id 	user_id 	edited_by 	created_at 	updated_at

        $sub_category = new ProductSubCategory();

        $sub_category->sub_category = ucfirst($request->input('sub_category'));
        $sub_category->sub_category_image = $fileNameToStore;
        $sub_category->obfuscator = $obfuscator;
        $sub_category->category_id = $request->input('category');
        $sub_category->user_id = $user_id;

        // Save record to database
        if ($sub_category->save()) {
            return redirect('sub_categories')->with('success', 'Sub category details have been saved');
        } else {
            return redirect('sub_categories')->with('error', 'Sub category details have not been saved');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // print('working');
        $obfuscator = $id;

        // Retrieve sub category from database
        $sub_category = ProductSubCategory::where('obfuscator', $obfuscator)->first();
        // print($sub_category);

        // Retrieve associated category from database
        $category = ProductSubCategory::where('obfuscator', $obfuscator)->first()->category;
        // print($category);exit;

        return view('sub_categories/show')->with(['category' => $category, 'sub_category' => $sub_category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obfuscator = $id;

        // Retrieve all categories from database
        $categories = ProductCategory::all();
        // print($categories);

        // Retrieve associated category from database
        $current_category = ProductSubCategory::where('obfuscator', $obfuscator)->first()->category;

        // Retrieve sub category from database
        $sub_category = ProductSubCategory::where('obfuscator', $obfuscator)->first();
        // print($sub_category);

        return view('sub_categories/edit')->with(['sub_category' => $sub_category, 'categories' => $categories, 'current_category' => $current_category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category' => 'required',
            'sub_category' => 'required'
        ]);
        // print($request->category);
        // print($request->sub_category);

        $obfuscator = $id;

        // Get the user's ID
        $edited_by = auth()->user()->id;

        if ($request->hasFile('picture')) {

            // get file name with extension
            $fileNameWithExt = $request->file('picture')->getClientOriginalName();

            // Get file name only
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get the file extension only
            $extension = $request->file('picture')->getClientOriginalExtension();

            // Create file name to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            // Upload image
            $path = $request->file('picture')->storeAs('public/uploads/sub_categories', $fileNameToStore);

            // id 	sub_category 	sub_category_image 	obfuscator 	category_id 	user_id 	edited_by 	created_at 	updated_at

            $sub_category = ProductSubCategory::where('obfuscator', $obfuscator)->first();

            // Delete current picture from database
            $picture = $sub_category->sub_category_image;

            if (Storage::delete('public/uploads/sub_categories/'.$picture)) {
                $sub_category->sub_category = ucfirst($request->input('sub_category'));
                $sub_category->sub_category_image = $fileNameToStore;
                $sub_category->category_id = $request->input('category');
                $sub_category->edited_by = $edited_by;

                if ($sub_category->save()) {
                    return redirect('sub_categories')->with('success', 'Sub Category details have been updated');
                } else {
                    return redirect('sub_categories')->with('error', 'Sub Category details have not been updated');
                }
            }

        } else {

            $fileNameToStore = 'nofile.png';

            $sub_category = ProductSubCategory::where('obfuscator', $obfuscator)->first();

            if ($sub_category->sub_category_image == 'nofile.png' || $sub_category->sub_category_image == NULL || $sub_category->sub_category_image == '') {
                $sub_category->sub_category_image = $fileNameToStore;
            }
            $sub_category->sub_category = ucfirst($request->input('sub_category'));
            $sub_category->category_id = $request->input('category');
            $sub_category->edited_by = $edited_by;

            if ($sub_category->save()) {
                return redirect('sub_categories')->with('success', 'Sub Category details have been updated');
            } else {
                return redirect('sub_categories')->with('error', 'Sub Category details have not been updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Change availability Status
        $obfuscator = $id;

        // Retrieve record from database
        $sub_category = ProductSubCategory::where('obfuscator', $obfuscator)->first();
        // print($sub_category);

        if ($sub_category->available == TRUE) {

            $sub_category->available = FALSE;

            if ($sub_category->save()) {
                return redirect('sub_categories')->with('success', 'The sub category -'.$sub_category->sub_category.' has been disabled');
            } else {
                return redirect('sub_categories')->with('error', 'The sub category -'.$sub_category->sub_category.' has not been disabled');
            }

        } else {

            $sub_category->available = TRUE;

            if ($sub_category->save()) {
                return redirect('sub_categories')->with('success', 'The sub category -'.$sub_category->sub_category.' has been enabled');
            } else {
                return redirect('sub_categories')->with('error', 'The sub category -'.$sub_category->sub_category.' has not been enabled');
            }

        }

    }
}
