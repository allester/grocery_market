<?php

namespace App\Http\Controllers\Customer\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;

use App\Brand;

class LoginController extends Controller
{

    /**
     * This trait has all the login throttling functionality
     */
    use ThrottlesLogins;

    /**
     * Only guests for "customer" guard are allowed except
     * for logout
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('guest:customer')->except('logout');
    }

    /**
     * Max login attempts allowed
     */
    public $maxAttempts = 5;

    /**
     * Number of minutes to lock the login
     */
    public $decayMinutes = 3;

    /**
     * Username used in ThrottlesLogins trait
     */
    public function username(){
        return 'email';
    }

    /**
     * Show the login form
     *
     * @return \Illuminate\Http\Response
     */

    public function showLoginForm(){

        $brands = Brand::all();
        // print($brands);

        return view('auth.login', [
            'title' => 'Customer Login',
            'loginRoute' => 'customer.login',
            'forgotPasswordRoute' => 'customer.password.request',
            'brands' => $brands
        ]);

    }

    /**
     * Login the customer.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {
        //Validation...
        $this->validator($request);

        // Check if the user has too many login attempts
        if ($this->hasTooManyLoginAttempts($request)) {
            // Fire the lockout event
            $this->fireLockoutEvent($request);

            // redirect the user back after lockout
            return $this->sendLockoutResponse($request);
        }

        //Login the customer...
        if (Auth::guard('customer')->attempt($request->only('email', 'password'), $request->filled('remember'))) {
            // Authentication passed...
            return redirect()->back()->with('status', 'You are logged in');
            // return redirect()->intended(route('customer.profile'))->with('status', 'You are logged in!');
        }

        // Keep track of login attempts from the user
        $this->incrementLoginAttempts($request);

        //Redirect the customer...
        return $this->loginFailed();
    }

    /**
     * Logout the customer.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
      //logout the customer...
      Auth::guard('customer')->logout();
      return redirect()->route('customer.login')->with('status', 'You have been logged out');
    }

    /**
     * Validate the form data.
     *
     * @param \Illuminate\Http\Request $request
     * @return
     */
    private function validator(Request $request)
    {

        // Validation rules
        $rules = [
            'email' => 'required|email|exists:customers|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
        ];

        // custom validation error messages.
        $messages = [
            'email.exists' => 'These credentials do not match our records.'
        ];

        // validate the request
        $request->validate($rules, $messages);

    }

    /**
     * Redirect back after a failed login.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    private function loginFailed()
    {
        return redirect()->back()->withInput()->with('error', 'Login failed, please try again!');
    }
}
