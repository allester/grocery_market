<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth:customer');
    }

    /**
     * Show Customer Profile
     *
     * @return \Illuminate\Http\Response
     */

     public function index(){
        return view('customer.profile');
     }
}
