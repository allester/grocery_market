<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use App\Brand;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;

class BrandsController extends Controller
{

    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retrieve all brands from database
        $brands = Brand::paginate(10);
        // print($brands);

        return view('brands/index')->with('brands', $brands);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // Retrieve all categories
        $product_categories = ProductCategory::all();
        // print($product_categories);

        return view('brands/create')->with('product_categories', $product_categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'brand' => 'required',
            'product_category' => 'required'
        ]);
        // print($request);

        // Get the user ID
        $user_id = auth()->user()->id;

        $obfuscator = Str::random(5);

        if ($request->hasFile('picture')) {

            // Get file name with extension
            $fileNameWithExt = $request->file('picture')->getClientOriginalName();

            // Get file name only
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get the file extension only
            $extension = $request->file('picture')->getClientOriginalExtension();

            // Create file name to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            // Upload image
            $path = $request->file('picture')->storeAs('public/uploads/brands', $fileNameToStore);

        } else {

            $fileNameToStore = 'nofile.pdf';

        }

        // 	id 	brand 	brand_logo 	category_id 	user_id 	edited_by 	created_at 	updated_at

        $brand = new Brand();

        $brand->brand = ucfirst($request->input('brand'));
        $brand->brand_logo = $fileNameToStore;
        $brand->category_id = $request->input('product_category');
        $brand->user_id = $user_id;
        $brand->obfuscator = $obfuscator;

        // Save record to database
        if ($brand->save()) {
            return redirect('brands')->with('success', 'Brand details have been saved');
        } else {
            return redirect('brands')->with('error', 'Brand details have not been saved');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // print($id);

        $obfuscator = $id;

        // Get brand from database
        $brand = Brand::where('obfuscator', $obfuscator)->first();
        // print($brand);exit;

        // Get category from database
        $category_id = $brand->category_id;

        $category = ProductCategory::find($category_id);
        // print($category);

        return view('brands/show')->with(['brand' => $brand, 'category' => $category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // print($id);

        $obfuscator = $id;

        // Retrieve brand details
        $brand = Brand::where('obfuscator', $obfuscator)->first();
        // print($brand);

        // print($brand->category_id);

        $category_id = $brand->category_id;

        // Get the current category
        $current_category = ProductCategory::find($category_id);
        // print($current_category);

        // Retrieve all categories
        $product_categories = ProductCategory::all();
        // print($product_categories);

        return view('brands/edit')->with(['brand' => $brand, 'product_categories' => $product_categories, 'current_category' => $current_category]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'brand' => 'required',
            'product_category' => 'required'
        ]);

        // print($request);exit;

        $brand_id = $id;

        // Get the user's ID
        $edited_by = auth()->user()->id;

        if ($request->hasFile('picture')) {

            // get file name with extension
            $fileNameWithExt = $request->file('picture')->getClientOriginalName();

            // Get file name only
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get the file extension only
            $extension = $request->file('picture')->getClientOriginalExtension();

            // Create file name to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            // Upload image
            $path = $request->file('picture')->storeAs('public/uploads/brands',$fileNameToStore);

            // 	id 	brand 	brand_logo 	category_id 	user_id 	edited_by 	created_at 	updated_at

            $brand = Brand::find($brand_id);

            // Delete current brand logo
            $logo = $brand->brand_logo;

            if (Storage::delete('public/uploads/brands/'.$logo)) {

                $brand->brand = ucfirst($request->input('brand'));
                $brand->brand_logo = $fileNameToStore;
                $brand->category_id = $request->input('product_category');
                $brand->edited_by = $edited_by;

                // Save changes to database
                if ($brand->save()) {
                    return redirect('brands')->with('success', 'Brand details have been updated');
                } else {
                    return redirect('brands')->with('error', 'Brand details have not been updated');
                }

            }

        } else {

            $fileNameToStore = 'nofile.pdf';

            $brand = Brand::find($brand_id);

            $brand->brand = ucfirst($request->input('brand'));
            $brand->category_id = $request->input('product_category');
            $brand->edited_by = $edited_by;

            // Save changes to database
            if ($brand->save()) {
                return redirect('brands')->with('success', 'Brand details have been updated');
            } else {
                return redirect('brands')->with('error', 'Brand details have not been updated');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // print($id);

        $obfuscator = $id;

        // Retrieve record from database
        $brand = Brand::where('obfuscator', $obfuscator)->first();
        // print($brand);

        if ($brand) {
            if ($brand->delete()) {
                return redirect('brands')->with('success', 'Brand details have been deleted');
            } else {
                return redirect('brands')->with('error', 'Brand details have not been deleted');
            }
        }

    }
}
