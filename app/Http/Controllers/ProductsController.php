<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory;
use App\Brand;
use App\ProductSubCategory;
use App\StockTracking;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;

class ProductsController extends Controller
{

    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Retrieve all products from database
        $products = Product::all();
        // print($products);

        return view('products/index')->with('products', $products);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // retrieve all categories from database
        $product_categories = ProductCategory::all();
        // print($product_categories);

        // Retrieve all sub categories from database
        $sub_categories = ProductSubCategory::all();
        // print($sub_categories);

        // Retrieve all brands from database
        $brands = Brand::all();
        // print($brands);

        return view('products/create')->with(['product_categories' => $product_categories, 'brands' => $brands, 'sub_categories' => $sub_categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required',
            'price' => 'required',
            'product_category' => 'required',
            'sub_category' => 'required',
        ]);
        // print($request);exit;

        // Get the user ID
        $user_id = auth()->user()->id;

        $obfuscator = Str::random(5);

        if ($request->hasFile('picture')) {

            // Get file name with extension
            $fileNameWithExt = $request->file('picture')->getClientOriginalName();

            // Get file name only
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get the file extension only
            $extension = $request->file('picture')->getClientOriginalExtension();

            // Create file name to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            // Upload image
            $path = $request->file('picture')->storeAs('public/uploads/products', $fileNameToStore);

        } else {

            $fileNameToStore = 'nofile.pdf';

        }

        $stock_tracking = $request->input('track_stock');
        $track_stock = FALSE;
        if ($stock_tracking == 'Yes') {
            $track_stock = TRUE;
        } else{
            $track_stock = FALSE;
        }

        // STEPS:

        // 1. Save product information to database
        // 2. Retrieve id of saved product after insertion into database
        // 3. Record initial stock if stock is to be tracked
        // 4. Retrieve id of stock level/ record after insertion into database
        // 5. Find the newly inserted product.
        // 6. Update record of product with stock_tracking_id to ensure stock is monitored

        // id 	product_name 	product_image 	price 	track_stock 	description 	category_id 	user_id 	edited_by 	stock_tracking_id 	brand_id 	created_at 	updated_at

        $product = new Product();

        $product->product_name = ucfirst($request->input('product_name'));
        $product->product_image = $fileNameToStore;
        $product->price = $request->input('price');
        $product->description = 'Test description';
        $product->image_url = $request->input('image_url');
        $product->category_id = $request->input('product_category');
        $product->sub_category_id = $request->input('sub_category');
        $product->track_stock = FALSE;
        $product->user_id = $user_id;
        $product->obfuscator = $obfuscator;

        if ($product->save()) {
            return redirect('products')->with('success', 'The product has been inserted');
        } else {
            return redirect('products')->with('error', 'The product has not been inserted');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // print($id);

        $obfuscator = $id;

        // Get product from database
        $product = Product::where('obfuscator', $obfuscator)->first();
        // print($product->brand);

        $category_id = $product->category_id;
        $brand_id = $product->brand_id;

        // Get the category
        $category = ProductCategory::find($category_id);
        // print($category);

        // Get the brand
        $brand = Brand::find($brand_id);
        // print($brand);

        return view('products/show')->with(['product' => $product, 'category' => $category, 'brand' => $brand]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // print($id);

        $obfuscator = $id;

        // Retrieve product details
        $product = Product::where('obfuscator', $obfuscator)->first();
        // print($product->category_id);

        $category_id = $product->category_id;
        $brand_id = $product->brand_id;

        // Retrieve product's category
        $product_category = ProductCategory::find($category_id);
        // print($product_category);

        // Retrieve product's brand
        $product_brand = Brand::find($brand_id);
        // print($product_brand);


        // Retrieve all categories
        $categories = ProductCategory::all();
        // print($categories);

        // Retrieve all brands
        $brands = Brand::all();
        // print($brands);

        // Retrieve stock details
        $stock_tracking = StockTracking::find($product->stock_tracking_id);
        // print($stock_tracking);

        return view('products/edit')->with(['product' => $product, 'product_category' => $product_category, 'product_brand' => $product_brand, 'categories' => $categories, 'brands' => $brands, 'stock_tracking' => $stock_tracking]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_name' => 'required',
            'price' => 'required',
            'product_category' => 'required',
            'brand' => 'required',
            'product_description' => 'required'
        ]);
        // print($id);
        // print($request);exit;

        $obfuscator = $id;

        // Get the user's ID
        $edited_by = auth()->user()->id;

        if ($request->hasFile('picture')) {

            // Get file name with extension
            $fileNameWithExt = $request->file('picture')->getClientOriginalName();

            // Get file name only
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get the file extension only
            $extension = $request->file('picture')->getClientOriginalExtension();

            // Create file name to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            // Upload image
            $path = $request->file('picture')->storeAs('public/uploads/products', $fileNameToStore);

            // id 	product_name 	product_image 	price 	track_stock 	description 	category_id 	user_id 	edited_by 	stock_tracking_id 	brand_id 	created_at 	updated_at

            $product = Product::where('obfuscator', $obfuscator)->first();

            // To be added later:
                // 1. Check if file exists and delete before making changes

                $product->product_name = ucfirst($request->input('product_name'));
                $product->product_image = $fileNameToStore;
                $product->price = $request->input('price');
                $product->description = ucfirst($request->input('product_description'));
                $product->image_url = $request->input('image_url');
                $product->category_id = $request->input('product_category');
                // $product->brand_id = $request->input('brand');
                $product->edited_by = $edited_by;

                if ($product->save()) {
                    return redirect('products')->with('success', 'Product details have been updated');
                } else {
                    return redirect('products')->with('error', 'Product details have not been updated');
                }


        } else {

            $fileNameToStore = 'nofile.pdf';

            $product = Product::where('obfuscator', $obfuscator)->first();

            $product->product_name = ucfirst($request->input('product_name'));
            $product->price = $request->input('price');
            $product->description = ucfirst($request->input('product_description'));
            $product->category_id = $request->input('product_category');
            $product->brand_id = $request->input('brand');
            $product->edited_by = $edited_by;

            if ($product->save()) {
                return redirect('products')->with('success', 'Product details have been updated');
            } else {
                return redirect('products')->with('error', 'Product details have not been updated');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // print($id);

        $obfuscator = $id;

        $product = Product::where('obfuscator', $obfuscator)->first();

        // Change availability of product
        if ($product->available == TRUE) {
            $product->available = FALSE;
            // Product is unavailable
            if ($product->save()) {
                return redirect('products')->with('success', 'Product is no longer available on the website');
            } else {
                return redirect('products')->with('error', 'Product status has not been modified');
            }
        } else {
            $product->available = TRUE;
            // Product is available
            if ($product->save()) {
                return redirect('products')->with('success', 'Product is now available on the website');
            } else {
                return redirect('products')->with('error', 'Product statsu has not been modified');
            }
        }

    }
}
