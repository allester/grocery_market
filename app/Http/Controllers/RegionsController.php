<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Region;

use Illuminate\Support\Str;

class RegionsController extends Controller
{

    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Retrieve all regions from database
        $regions = Region::all();
        // print($regions);

        return view('regions/index')->with('regions', $regions);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('regions/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'region' => 'required'
        ]);
        // print($request);exit;

        // id 	region 	obfuscator 	user_id 	edited_by 	created_at 	updated_at

        // Get the user ID
        $user_id = auth()->user()->id;

        $obfuscator = Str::random(5);

        $region = new Region();

        $region->region = ucfirst($request->input('region'));
        $region->obfuscator = $obfuscator;
        $region->user_id = $user_id;

        if ($region->save()) {
            return redirect('regions')->with('success', 'Region has been added');
        } else {
            return redirect('regions')->with('error', 'Region has not been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // print($id);

        $obfuscator = $id;

        // Get region from database
        $region = Region::where('obfuscator', $obfuscator)->first();
        // print($region);

        return view('regions/show')->with('region', $region);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // print($id);

        $obfuscator = $id;

        // Retrieve region details
        $region = Region::where('obfuscator', $obfuscator)->first();
        // print($region);

        return view('regions/edit')->with('region', $region);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'region' => 'required'
        ]);
        // print($id);exit;

        $obfuscator = $id;

        // Get the user's ID
        $edited_by = auth()->user()->id;

        $region = Region::where('obfuscator', $obfuscator)->first();
        // print($region);exit;

        $region->region = ucfirst($request->input('region'));
        $region->edited_by = $edited_by;

        // Save changes to database
        if ($region->save()) {
            return redirect('regions')->with('success', 'Region has been updated successfully');
        } else {
            return redirect('regions')->with('error', 'Region has not been updated');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // print($id);

        $obfuscator = $id;

        // Retrieve record from database
        $region = Region::where('obfuscator', $obfuscator)->first();
        // print($region);

        $current_validity = $region->validity;
        // print($current_validity);

        $validity = '';

        // Change validity
        if ($current_validity == TRUE) {
            $validity = FALSE;

            $region->validity = $validity;

            // Save changes to database
            if ($region->save()) {
                return redirect('regions')->with('success', 'Region has been disabled');
            } else {
                return redirect('regions')->with('error', 'Region has not been disabled');
            }

        } else {
            $validity = TRUE;

            $region->validity = $validity;

            if ($region->save()) {
                return redirect('regions')->with('success', 'Region has been enabled');
            } else {
                return redirect('regions')->with('error', 'Region has not been enabled');
            }

        }

    }
}
