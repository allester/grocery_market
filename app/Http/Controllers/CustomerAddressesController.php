<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\CustomerAddress;
use App\Customer;

class CustomerAddressesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (false == Auth::guard('customer')->check()) {
            return redirect()->route('customer.login');
        } else {

            return view('customer_addresses.create');

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'billing_address' => 'required'
        ]);
        // print($request);exit;

        $customer_id = Auth::guard('customer')->user()->id;

        $customer_address = new CustomerAddress();

        $customer_address->address = $request->input('billing_address');
        $customer_address->customer_id = $customer_id;

        if ($customer_address->save()) {
            return redirect('customer/profile')->with('success', 'Billing address has been added');
        } else {
            return redirect('customer/profile')->with('error', 'Billing address has not been added');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if (false == Auth::guard('customer')->check()) {
            return redirect()->route('customer.login');
        } else {

            // print($id);

            $obfuscator = $id;

            // Retrieve details of customer
            $customer = Customer::where('obfuscator', $obfuscator)->first();
            // print($customer);

            $customer_id = $customer->id;
            // print($customer_id);

            $billing_address = CustomerAddress::where('customer_id', $customer_id)->first();
            // print($billing_address);

            return view('customer_addresses.edit')->with('billing_address', $billing_address);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (false == Auth::guard('customer')->check()) {
            return redirect()->route('customer.login');
        } else {

            $this->validate($request, [
                'billing_address' => 'required'
            ]);
            // print($id);
            // print($request);exit;

            $billing_address = CustomerAddress::find($id);
            // print($billing_address);

            $billing_address->address = $request->input('billing_address');

            if ($billing_address->save()) {
                return redirect('customer/profile')->with('success', 'Billing address has been updated');
            } else {
                return redirect('customer/profile')->with('error', 'Billing address has not been updated');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
