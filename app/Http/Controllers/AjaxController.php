<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use App\ProductSubCategory;
use App\Product;
use App\Region;
use App\Area;

class AjaxController extends Controller
{

    public function get_sub_categories_per_category(Request $request){

        // print($request->elemID);exit;

        $obfuscator = $request->elemID;

        $product_category = ProductCategory::where('obfuscator', $obfuscator)->first();
        // print($product_category);exit;

        $category_id = $product_category->id;

        // Retrieve associated sub_categories
        $sub_categories = ProductSubCategory::where('category_id', $category_id)->get();
        // print($sub_categories);exit;

        return response()->json($sub_categories);


    }

    public function get_products_per_sub_category(Request $request){

        // print($request->elemID);exit;

        $obfuscator = $request->sub_category_id;

        $product_sub_category = ProductSubCategory::where('obfuscator', $obfuscator)->first();
        // print($product_category);exit;

        $sub_category_id = $product_sub_category->id;
        // print($category_id);exit;

        // Retrieve associated products
        $products = Product::where('sub_category_id', $sub_category_id)->get();
        // print($products);exit;
        // print(json_encode($products));

        return response()->json($products);

    }

    public function areas_per_region(Request $request){

        $obfuscator = $request->region_id;

        $region = Region::where('obfuscator', $obfuscator)->first();

        $region_id = $region->id;

        // Retrieve all areas under this region
        $areas = Area::where('region_id', $region_id)->get();

        return response()->json($areas);

    }

    public function change_featured_status(Request $request){

        $obfuscator = $request->product_category;

        $product_category = ProductCategory::where('obfuscator', $obfuscator)->first();
        // print($product_category);exit;

        $success_message = 'The category\'s featured status has been updated';

        $error_message = 'The category\'s featured status has not been updated';

        if ($product_category->featured == TRUE) {
            $product_category->featured = FALSE;
            if ($product_category->save()) {
                return response()->json($success_message);
            } else {
                return response()->json($error_message);
            }
        } else {
            $product_category->featured = TRUE;
            if ($product_category->save()) {
                return response()->json($success_message);
            } else {
                return response()->json($error_message);
            }
        }

    }

    public function get_sub_categories_per_product_category(Request $request){

        $category_id = $request->category_id;

        // Retrieve associated sub categories
        $sub_categories = ProductSubCategory::where('category_id', $category_id)->get();

        return response()->json($sub_categories);


    }

}
