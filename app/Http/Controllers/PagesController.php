<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class PagesController extends Controller
{

    public function order_success(){

        if (false == Auth::guard('customer')->check()) {
            return redirect()->back()->with('error', 'Unauthorised page, please complete an order first');
        } else {

            return view('orders/success');

        }
    }

}
