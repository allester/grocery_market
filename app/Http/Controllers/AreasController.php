<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Region;
use App\Area;

use Illuminate\Support\Str;

class AreasController extends Controller
{

    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retrieve all areas from database
        $areas = Area::orderBy('area', 'ASC')->paginate(20);
        // print($areas);

        return view('areas/index')->with('areas', $areas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Retrieve all regions from database
        $regions = Region::all();
        // print($regions);

        return view('areas/create')->with('regions', $regions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'region' => 'required',
            'area' => 'required'
        ]);
        // print($request);exit;

        // id 	area 	obfuscator 	region_id 	user_id 	edited_by 	created_at 	updated_at

        // Get the user's ID
        $user_id = auth()->user()->id;

        $obfuscator = Str::random(5);

        $area = new Area();

        $area->area = ucfirst($request->input('area'));
        $area->obfuscator = $obfuscator;
        $area->region_id = $request->input('region');
        $area->user_id = $user_id;

        if ($area->save()) {
            return redirect('areas')->with('success', 'Area has been added');
        } else {
            return redirect('areas')->with('error', 'Area has not been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // print($id);

        $obfuscator = $id;

        // Get area from database
        $area = Area::where('obfuscator', $obfuscator)->first();
        // print($area);

        $region_id = $area->region_id;
        // print($region_id);

        // Get region from database
        $region = Region::find($region_id);
        // print($region);

        return view('areas/show')->with(['area' => $area, 'region' => $region]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // print($id);

        $obfuscator = $id;

        // Retrieve area details
        $area = Area::where('obfuscator', $obfuscator)->first();
        // print($area);

        $region_id = $area->region_id;

        // Retrieve associated region
        $current_region = Region::find($region_id);
        // print($current_region);

        // Retrieve all regions from database
        $regions = Region::all();
        // print($regions);

        return view('areas/edit')->with(['area' => $area, 'current_region' => $current_region, 'regions' => $regions]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'region' => 'required',
            'area' => 'required'
        ]);
        // print($request);exit;

        $obfuscator = $id;

        // Get the user's ID
        $edited_by = auth()->user()->id;

        // id 	area 	obfuscator 	region_id 	user_id 	edited_by 	created_at 	updated_at

        $area = Area::where('obfuscator', $obfuscator)->first();
        // print($area);

        $area->area = ucfirst($request->input('area'));
        $area->region_id = $request->input('region');
        $area->edited_by = $edited_by;

        // Save changes to database
        if ($area->save()) {
            return redirect('areas')->with('success', 'Area has been updated successfully');
        } else {
            return redirect('areas')->with('error', 'Area has not been updated');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // print($id);

        $obfuscator = $id;

        // Retrieve record from database
        $area = Area::where('obfuscator', $obfuscator)->first();
        // print($area);

        $current_validity = $area->validity;
        // print($current_validity);

        $validity = '';

        // Change validity
        if ($current_validity == TRUE) {
            $validity = FALSE;

            $area->validity = $validity;

            // Save changes to database
            if ($area->save()) {
                return redirect('areas')->with('success', 'Area has been disabled');
            } else {
                return redirect('areas')->with('error', 'Area has not been disabled');
            }

        } else {
            $validity = TRUE;

            $area->validity = $validity;

            if ($area->save()) {
                return redirect('areas')->with('success', 'Area has been enabled');
            } else {
                return redirect('areas')->with('error', 'Area has not been enabled');
            }

        }
    }
}
