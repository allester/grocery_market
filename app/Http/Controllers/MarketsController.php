<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Market;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;

use Auth;

class MarketsController extends Controller
{

    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $markets = Market::paginate(10);

        return view('markets.index')->with('markets', $markets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('markets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'market' => 'required'
        ]);

        // Get the user ID
        $user_id = auth()->user()->id;

        // Generate obfuscator
        $obfuscator = Str::random(5);
        // print($obfuscator);exit;

        // print($request);
        // exit;

        if ($request->hasFile('picture')) {

            // Get file name with extension
            $fileNameWithExt = $request->file('picture')->getClientOriginalName();

            // Get file name only
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get the file extension only
            $extension = $request->file('picture')->getClientOriginalExtension();

            // Create file name to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;

            // Upload image
            $path = $request->file('picture')->storeAs('public/uploads/markets', $fileNameToStore);
        } else {

            $fileNameToStore = 'nofile.png';
        }

        // Save market to database
        $market = new Market();

        $market->market = ucwords($request->input('market'));
        $market->image = $fileNameToStore;
        $market->obfuscator = $obfuscator;
        $market->image_url = $request->input('image_url');

        // Save record to database
        if ($market->save()) {
            return redirect('markets')->with('success', 'The market has been added');;
        } else {
            return redirect('markets')->with('error', 'The market has not been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // print($id);

        $market = Market::where('obfuscator', $id)->first();
        // print($market);

        return view('markets.show')->with(['market' => $market]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obfuscator = $id;

        // Retrieve category from database
        $market = Market::where('obfuscator', $obfuscator)->first();
        // print($product_category);

        return view('markets/edit')->with('market', $market);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'market' => 'required'
        ]);

        $market_id = $id;

        if ($request->hasFile('picture')) {

            // get file name with extension
            $fileNameWithExt = $request->file('picture')->getClientOriginalName();

            // Get file name only
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get the file extension only
            $extension = $request->file('picture')->getClientOriginalExtension();

            // Create file name to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;

            // Upload image
            $path = $request->file('picture')->storeAs('public/uploads/categories', $fileNameToStore);

            // {{-- id 	category 	category_image 	user_id 	edited_by 	created_at 	updated_at --}}

            $market = Market::where('obfuscator', $market_id)->first();

            // Delete current picture from database/ storage
            $picture = $market->image;

            if (Storage::delete('public/uploads/markets/' . $picture)) {

                $market->market = ucfirst($request->input('market'));
                $market->image = $fileNameToStore;
                $market->image_url = $request->input('image_url');

                if ($market->save()) {
                    return redirect('markets')->with('success', 'Market details have been updated');
                } else {
                    return redirect('market')->with('error', 'Market details have not been updated');
                }
            }
        } else {

            $fileNameToStore = 'nofile.pdf';

            $market = Market::where('obfuscator', $market_id)->first();

            $market->market = ucfirst($request->input('market'));
            $market->image = $fileNameToStore;
            $market->image_url = $request->input('image_url');

            if ($market->save()) {
                return redirect('markets')->with('success', 'Market details have been updated');
            } else {
                return redirect('markets')->with('error', 'Market details have not been updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $obfuscator = $id;

        // Retrieve record
        $market = Market::where('obfuscator', $obfuscator)->first();

        if ($market) {
            if ($market->delete()) {
                return redirect('markets')->with('success', 'Market details have been deleted');
            } else {
                return redirect('markets')->with('error', 'Market details have not been deleted');
            }
        }
    }
}
