<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use App\ProductSubCategory;
use App\Product;
use App\Order;
use App\Brand;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // Count all categories
        $product_categories = ProductCategory::all();
        // print($product_categories);
        $category_count = count($product_categories);

        // Count all sub categories
        $sub_categories = ProductSubCategory::all();
        // print($sub_categories);
        $sub_category_count = count($sub_categories);

        // Count all products
        $products = Product::all();
        $product_count = count($products);

        // Count all brands
        $brand = Brand::all();
        $brand_count = count($brand);

        // Count all orders
        $orders = Order::all();
        $order_count = count($orders);

        $pending_orders = 'Pending';
        $processed_orders = 'Processed';
        $dispatched_orders = 'Dispatched';
        $completed_orders = 'Completed';

        // All Brands
        $all_brands = Brand::all();

        // Get all orders
        $pending_orders = Order::where('order_status', $pending_orders)->with('customer', 'products')->get();
        // print($pending_orders);
        $pending_orders_count = count($pending_orders);

        $processed_orders = Order::where('order_status', $processed_orders)->with('customer', 'products', 'order_delivery')->get();
        // print($processed_orders);
        $processed_order_count = count($processed_orders);

        $dispatched_orders = Order::where('order_status', $dispatched_orders)->with('customer', 'products', 'order_delivery')->get();
        $dispatched_order_count = count($dispatched_orders);

        $completed_orders = Order::where('order_status', $completed_orders)->with('customer', 'products')->get();
        // print($completed_orders);
        $completed_order_count = count($completed_orders);

        return view('dashboard')->with(['category_count' => $category_count, 'sub_category_count' => $sub_category_count, 'product_count' => $product_count, 'brand_count' => $brand_count, 'order_count' => $order_count, 'pending_orders' => $pending_orders, 'processed_orders' => $processed_orders, 'dispatched_orders' => $dispatched_orders, 'completed_orders' => $completed_orders, 'pending_orders_count' => $pending_orders_count, 'processed_orders_count' => $processed_order_count, 'dispatched_orders_count' => $dispatched_order_count, 'completed_orders_count' => $completed_order_count, 'all_brands' => $all_brands]);
    }
}
