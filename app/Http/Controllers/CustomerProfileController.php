<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Customer;
use App\CustomerAddress;
use App\Brand;

class CustomerProfileController extends Controller
{

    /**
     * Show the customer's profile
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function profile(){

        if (false == Auth::guard('customer')->check()) {
            return redirect()->route('customer.login');
        } else {

            $customer_id = Auth::guard('customer')->user()->id;

            // Retrieve customer's details
            $customer = Customer::find($customer_id);
            // print($customer);

            // Retrieve customer's addresses
            $customer_address = CustomerAddress::where('customer_id', $customer_id)->first();

            $customer_has_address = FALSE;

            if ($customer_address) {
                $customer_has_address = TRUE;
            } else {
                $customer_has_address = FALSE;
            }

            $brands = Brand::all();
            // print($brands);exit;

            return view('customer.profile')->with(['customer' => $customer, 'customer_address' => $customer_address, 'customer_has_address' => $customer_has_address, 'brands' => $brands]);

        }

    }

    public function edit($id){

        if (false == Auth::guard('customer')->check()) {
            return redirect()->route('customer.login');
        } else {

            // print($id);exit;

            $obfuscator = $id;

            $customer = Customer::where('obfuscator', $obfuscator)->first();
            // print($customer);

            return view('customer.edit')->with('customer', $customer);

        }

    }

    public function update(Request $request, $id){

        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

        //  id 	first_name 	last_name 	user_name 	email 	obfuscator 	email_verified_at 	password 	remember_token 	created_at 	updated_at 	phone_number

        // print($id);exit;
        $obfuscator = $id;
        $no_number = "No Number";

        $customer_id = Auth::guard('customer')->user()->id;

        $customer = Customer::where('obfuscator', $obfuscator)->first();
        // print($customer);

        $customer->first_name = $request->input('first_name');
        $customer->last_name = $request->input('last_name');
        $customer->user_name = $request->input('user_name');
        if ($request->input('phone_number') !== '') {
            $customer->phone_number = $request->input('phone_number');
        } else {
            $customer->phone_number = 'No Number';
        }

        if ($customer->save()) {
            return redirect('customer/profile')->with('success', 'Profile has been updated');
        } else {
            return redirect('customer/profile')->with('error', 'Profile has not been updated');
        }

    }

}
