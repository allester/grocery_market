<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Order;
use App\User;
use App\Customer;
use App\Product;
use App\OrderProduct;
use App\DeliveryTeam;
use App\Region;
use App\Area;
use Cart;

use Carbon\Carbon;

use Illuminate\Support\Str;

class OrdersController extends Controller
{

    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pending_orders = 'Pending';
        $processed_orders = 'Processed';
        $dispatched_orders = 'Dispatched';
        $completed_orders = 'Completed';

        // Get all orders
        $pending_orders = Order::where('order_status', $pending_orders)->with('customer', 'products')->get();
        // print($pending_orders);

        $processed_orders = Order::where('order_status', $processed_orders)->with('customer', 'products', 'order_delivery')->get();
        // print($processed_orders);

        $dispatched_orders = Order::where('order_status', $dispatched_orders)->with('customer', 'products', 'order_delivery')->get();

        $completed_orders = Order::where('order_status', $completed_orders)->with('customer', 'products')->get();
        // print($completed_orders);

        return view('orders/index')->with(['pending_orders'=> $pending_orders, 'processed_orders' => $processed_orders, 'dispatched_orders' => $dispatched_orders, 'completed_orders' => $completed_orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'country_code' => 'required',
            'mobile_phone' => 'required',
            'billing_address' => 'required',
            'region' => 'required',
            'area' => 'required'
        ]);
        // print($request->billing_address);
        // print($request->delivery_address);
        // $cart_items = Cart::content();
        // print($cart_items);
        // exit;

        $customer_id = Auth::guard('customer')->user()->id;

        $customer_phone_number = $request->input('mobile_phone');

        $customer = Customer::find($customer_id);

        if ($customer->phone_number == '' || $customer->phone_number == NULL || $customer->phone_number !== $customer_phone_number) {
            $customer->phone_number = $customer_phone_number;
            $customer->save();
        }

        // Register Order
        // id 	customer_id 	delivery_address 	created_at 	updated_at

        $obfuscator = Str::random(5);

        $region_obfuscator = $request->input('region');
        $area_obfuscator = $request->input('area');

        // Get details of region
        $region = Region::where('obfuscator', $region_obfuscator)->first();
        $region_id = $region->id;

        // Get details of area
        $area = Area::where('obfuscator', $area_obfuscator)->first();
        $area_id = $area->id;

        $order = new Order();

        $order->customer_id = $customer_id;
        if (!isset($_POST['delivery_address'])) {
            $order->delivery_address = $request->input('billing_address');
        } else {
            $order->delivery_address = $request->input('delivery_address');
        }
        $order->region_id = $region_id;
        $order->area_id = $area_id;
        $order->country_code = $request->input('country_code');
        $order->additional_info = $request->input('additional_info');
        $order->obfuscator = $obfuscator;
        $order->order_total = $request->input('order_total');
        // $order->market_id = $request->input('market');

        $order_id = '';
        $cart_items = Cart::content();

        // print($cart_items);exit;

        $order_id = '';

        if ($order->save()) {

            $order_id = $order->id;

            // Register order items
            // id 	order_id 	product_id 	created_at 	updated_at
            foreach ($cart_items as $item) {

                $obfuscator = $item->id;

                $product = Product::where('obfuscator', $obfuscator)->first();

                $order_product = new OrderProduct();

                $order_product->order_id = $order_id;
                $order_product->product_id = $product->id;
                $order_product->quantity = $item->qty;
                $order_product->total_cost = $item->subtotal;
                $order_product->market = $item->options->market;

                $order_product->save();
            }



            Cart::destroy();

            return redirect('successful_order');

        } else {
            return redirect()->back()->with('error', 'Error processing your order, please try again');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::check()) {
            return redirect('login');
        } else {

            // print($id);

            $obfuscator = $id;

            // Retrieve order details from database
            $order = Order::where('obfuscator', $obfuscator)->with('customer', 'products', 'delivery_person')->first();
            // print($order);exit;

            // Retrieve all members of delivery team
            $delivery_team = DeliveryTeam::all();
            // print($delivery_team);

            return view('orders/show')->with(['order' => $order, 'delivery_team' => $delivery_team]);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
