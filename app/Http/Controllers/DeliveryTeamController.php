<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DeliveryTeam;
use App\User;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;

class DeliveryTeamController extends Controller
{

    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Retrieve all delivery personnel from database
        $delivery_personnel = DeliveryTeam::orderBy('first_name', 'ASC')->paginate(10);
        // print($delivery_personnel);

        return view('delivery_team.index')->with('delivery_personnel', $delivery_personnel);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('delivery_team.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            // 'picture' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number_one' => 'required'
        ]);
        // print($request);exit;

        // Get the user's ID
        $user_id = auth()->user()->id;

        $obfuscator = Str::random(5);

        $identifier = Str::random(10);
        $person_identifier = strtoupper($identifier);

        // if ($request->hasFile('picture') && $request->hasFile('national_id')) {

        //     // Handle Person's Picture

        //     // Get file name with extension
        //     $fileNameWithExt1 = $request->file('picture')->getClientOriginalName();

        //     // Get file name only
        //     $filename1 = pathinfo($fileNameWithExt1, PATHINFO_FILENAME);

        //     // Get the file extension only
        //     $extension1 = $request->file('picture')->getClientOriginalExtension();

        //     // Create file name to store
        //     $fileNameToStore1 = $filename1.'_'.time().'.'.$extension1;

        //     // Upload image
        //     $path1 = $request->file('picture')->storeAs('public/uploads/delivery_personnel_picture', $fileNameToStore1);


        //     // Handle Person's National ID

        //     // Get file name with extension
        //     $fileNameWithExt2 = $request->file('national_id')->getClientOriginalName();

        //     // Get file name only
        //     $filename2 = pathinfo($fileNameWithExt2, PATHINFO_FILENAME);

        //     // Get the file extension only
        //     $extension2 = $request->file('national_id')->getClientOriginalExtension();

        //     // Create file name to store
        //     $fileNameToStore2 = $filename2.'_'.time().'.'.$extension2;

        //     // Upload image
        //     $path2 = $request->file('national_id')->storeAs('public/uploads/delivery_personnel_id', $fileNameToStore2);

        // } else {

        //     $fileNameToStore1 = 'nofile.png';
        //     $fileNameToStore2 = 'nofile.jpg';

        // }

        $fileNameToStore1 = 'nofile.png';
        $fileNameToStore2 = 'nofile.jpg';

        // New Delivery Person

        $delivery_person = new DeliveryTeam();

        $delivery_person->first_name = ucfirst($request->input('first_name'));
        $delivery_person->last_name = ucfirst($request->input('last_name'));
        $delivery_person->picture = $fileNameToStore1;
        $delivery_person->national_id = $fileNameToStore2;
        $delivery_person->phone_number_one = $request->input('phone_number_one');
        $phone_number_two = $request->input('phone_number_two');
        if ($phone_number_two !== NULL) {
            $delivery_person->phone_number_two = $phone_number_two;
        }
        $email = $request->input('email');
        if ($email !== NULL) {
            $delivery_person->email = $email;
        }
        $delivery_person->obfuscator = $obfuscator;
        $delivery_person->identifier = $person_identifier;
        $delivery_person->user_id = $user_id;

        if ($delivery_person->save()) {
            return redirect('delivery_team')->with('status', 'The delivery person has been registered');
        } else {
            return redirect('delivery_team')->with('error', 'The delivery person has not been registered');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // print($id);

        $obfuscator = $id;

        $delivery_person = DeliveryTeam::where('obfuscator', $obfuscator)->first();
        // print($delivery_person);

        return view('delivery_team.show')->with('delivery_person', $delivery_person);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // print($id);

        $obfuscator = $id;

        $delivery_person = DeliveryTeam::where('obfuscator', $obfuscator)->first();
        // print($delivery_person);

        return view('delivery_team.edit')->with('delivery_person', $delivery_person);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number_one' => 'required'
        ]);
        // print($request);
        // print($id);

        $obfuscator = $id;

        // Get the user's ID
        $edited_by = auth()->user()->id;

        $delivery_person = DeliveryTeam::where('obfuscator', $obfuscator)->first();

        $delivery_person->first_name = ucfirst($request->input('first_name'));
        $delivery_person->last_name = ucfirst($request->input('last_name'));
        $delivery_person->phone_number_one = $request->input('phone_number_one');
        $phone_number_two = $request->input('phone_number_two');
        if ($phone_number_two !== NULL) {
            $delivery_person->phone_number_two = $phone_number_two;
        }
        $email = $request->input('email');
        if ($email !== NULL) {
            $delivery_person->email = $email;
        }
        $delivery_person->edited_by = $edited_by;

        if ($delivery_person->save()) {
            return redirect('delivery_team')->with('status', 'The details have been updated');
        } else {
            return redirect()->back()->with('error', 'The details have not been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // print($id);

        $obfuscator = $id;

        $delivery_person = DeliveryTeam::where('obfuscator', $obfuscator)->first();
        // print($delivery_person);

        if ($delivery_person->delete()) {
            return redirect('delivery_team')->with('status', 'The delivery person\'s details have been deleted');
        } else {
            return redirect()->back()->with('error', 'The delivery person\'s details have not been deleted');
        }
    }
}
