<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory;
use App\ProductSubCategory;

class CategoryProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // print($id);

        $category = ProductCategory::where('obfuscator', $id)->first();

        $category_id = $category->id;

        // Get all products
        $products = Product::where('category_id', $category_id)->get();
        // print($products);

        return view('category_products/show')->with(['category' => $category, 'products' => $products]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function show_fruits()
    {

        $category = ProductCategory::where('category', 'Fruits')->first();
        // print($category);exit;

        $category_id = $category->id;

        // Get all products
        $products = Product::where('category_id', $category_id)->get();
        // print($products);

        return view('category_products/show_fruits')->with(['category' => $category, 'products' => $products]);
    }

    public function show_vegetables()
    {

        $category = ProductCategory::where('category', 'Vegetables')->first();
        // print($category);exit;

        $category_id = $category->id;

        // Get all products
        $products = Product::where('category_id', $category_id)->get();
        // print($products);

        return view('category_products/show_vegetables')->with(['category' => $category, 'products' => $products]);
    }

    public function show_meats()
    {
        $category = ProductCategory::where('category', 'Meats')->first();
        // print($category);exit;

        $category_id = $category->id;

        // Get all products
        $products = Product::where('category_id', $category_id)->get();
        // print($products);

        return view('category_products/show_meats')->with(['category' => $category, 'products' => $products]);
    }

    public function show_potatoes()
    {
        $category = ProductCategory::where('category', 'Potatoes')->first();
        // print($category);exit;

        $category_id = $category->id;

        // Get all products
        $products = Product::where('category_id', $category_id)->get();
        // print($products);

        return view('category_products/show_potatoes')->with(['category' => $category, 'products' => $products]);
    }
}
