<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendor;
use App\Market;

use Auth;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class VendorsController extends Controller
{

    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendors = Vendor::paginate(10);

        return view('vendors.index')->with('vendors', $vendors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Get all markets
        $markets = Market::all();

        return view('vendors.create')->with('markets', $markets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'vendor_name' => 'required',
            'phone' => 'required'
        ]);

        $obfuscator = Str::random(10);

        $vendor = new Vendor();

        $vendor->name = ucwords($request->input('vendor_name'));
        $vendor->email = $request->input('email');
        $vendor->phone = $request->input('phone');
        $vendor->image_url = $request->input('image_url');
        $vendor->obfuscator = $obfuscator;
        $vendor->market_id = $request->input('market');

        if ($vendor->save()) {
            return redirect('vendors')->with('success', 'The vendor has been added');
        } else {
            return redirect('vendors')->with('error', 'The vendor has not been added. Please try again');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obfuscator = $id;

        // Get all markets
        $markets = Market::all();

        // Retrieve vendor from database
        $vendor = Vendor::where('obfuscator', $obfuscator)->first();

        return view('vendors.edit')->with(['markets' => $markets, 'vendor' => $vendor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'vendor_name' => 'required',
            'phone' => 'required'
        ]);
        // print($request);
        // exit;

        $obfuscator = $id;

        $new_market_id = $request->input('market');
        $current_market_id = $request->input('current_market_id');

        $vendor = Vendor::where('obfuscator', $obfuscator)->first();

        $vendor->name = ucwords($request->input('vendor_name'));
        $vendor->email = $request->input('email');
        $vendor->phone = $request->input('phone');
        $vendor->image_url = $request->input('image_url');
        $vendor->obfuscator = $obfuscator;
        if ($new_market_id == NULL || $new_market_id == '') {
            $vendor->market_id = $current_market_id;
        } else {
            $vendor->market_id = $new_market_id;
        }

        if ($vendor->save()) {
            return redirect('vendors')->with('success', 'The vendor\'s details has been updated');
        } else {
            return redirect('vendors')->with('error', 'The vendor\'s details have not been updated. Please try again');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obfuscator = $id;

        $vendor = Vendor::where('obfuscator', $obfuscator)->first();

        if ($vendor) {
            if ($vendor->delete()) {
                return redirect('vendors')->with('success', 'The vendor\'s details have been deleted');
            } else {
                return redirect('markets')->with('error', 'The vendor\'s details have not been deleted');
            }
        }

    }
}
