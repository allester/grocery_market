<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Cart;
use App\Brand;

class CartController extends Controller
{
    public function add(Request $request)
    {

        // Retrieves the product ID
        $obfuscator = $request->input('id');
        // print($obfuscator);exit;

        $market = $request->input('market');

        // Retrieve product
        $product = Product::where('obfuscator', $obfuscator)->first();
        // print($product);exit;

        // Add product to shopping cart
        // Cart::add($product, 1);
        Cart::add(['id' => $product->obfuscator, 'name' => $product->product_name, 'qty' => 1, 'price' => $product->price, 'weight' => 0, 'options' => ['picture' => $product->product_image, 'image' => $product->image_url, 'market' => $market]]);

        return back()->with('success', "$product->product_name has been added to the cart");
    }

    public function cart(Request $request)
    {
        // print($request);exit;
        if (count(Cart::content()) > 0) {
            $cart_items = Cart::content();
            // print($cart_items);exit;

            // print(Cart::count());

            $brands = Brand::all();
            // print($brands);exit;

            return view('cart/cart')->with(['cart_items' => $cart_items, 'brands' => $brands]);
        } else {

            return back()->with('error', 'Please add some items to cart first');
        }
    }

    public function cart_increase_qty(Request $request)
    {
        // print($request);exit;

        $input = $request->all();
        // return response()->json($input);

        $row_id = $request->row_id;
        $selected_qty = $request->qty;

        // Get the item
        $item = Cart::get($row_id);
        Cart::update($row_id, $selected_qty);
        return response()->json($item);
    }

    public function update(Request $request, $id)
    {

        // print($request);
        // print($id);
        // print($request->quantity);

        $quantity = $request->input('quantity');

        $rowId = $id;

        // Get the item
        $item = Cart::get($rowId);
        Cart::update($rowId, $quantity);

        return redirect('cart');
    }

    public function update_cart(Request $request)
    {
        // print($request);
        // print($request->row_id);print($request->quantity);

        $rowID = $request->row_id;
        $quantity = $request->quantity;

        // // // Get the item
        $item = Cart::get($rowID);

        if (Cart::update($rowID, $quantity)) {
            return json_encode("Success");
        } else {
            return json_encode("Error");
        }
    }

    public function clear()
    {

        Cart::destroy();

        return redirect('/');
    }
}
