<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderDelivery;
use App\DeliveryTeam;
use App\DeliveryOrder;
use App\OrderProduct;

class OrderDeliveryController extends Controller
{
    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function process_order(Request $request, $id)
    {

        // print($request);
        // print($id);

        $obfuscator = $id;

        $order = Order::where('obfuscator', $obfuscator)->with('customer', 'products', 'market')->first();
        // print($order);exit;

        return view('order_delivery/process_order')->with('order', $order);
    }

    public function process_order_delivery(Request $request)
    {

        $this->validate($request, [
            // 'delivery_date' => 'required|date'
        ]);

        // print($request);

        $order_status = 'Processed';

        $user_id = auth()->user()->id;
        // print($user_id);

        $order_obfuscator = $request->input('order_id');
        $order = Order::where('obfuscator', $order_obfuscator)->with('customer', 'products', 'market')->first();;
        // print($order);exit;
        $order_id = $order->id;

        $order->order_status = $order_status;

        if ($order->save()) {

            $order_delivery = new OrderDelivery();
            $order_delivery->delivery_date = date("Y-m-d");
            $order_delivery->order_id = $order_id;
            $order_delivery->user_id = $user_id;

            if ($order_delivery->save()) {
                return redirect('orders')->with('status', 'Order has been processed successfully and delivery date set');
            } else {
                return redirect()->back()->with('error', 'Error processing order, please try again');
            }
        }
    }

    public function mark_unmark_item(Request $request)
    {

        // print($request);
        $item_id = $request->itemId;
        $order_id = $request->orderId;

        $order_product = OrderProduct::where(['order_id' => $order_id, 'product_id' => $item_id])->first();
        // print($order_product);

        $current_status = $order_product->item_picked;

        if ($current_status == FALSE) {
            // Check item
            $order_product->item_picked = TRUE;
        } else {
            $order_product->item_picked = FALSE;
        }

        if ($order_product->save()) {
            return json_encode("success");
        } else {
            return json_encode("error");
        }
    }

    public function dispatch_order(Request $request)
    {

        $this->validate($request, [
            'delivery_person' => 'required',
            'order_dispatch' => 'required'
        ]);
        // print($request);exit;

        // Get the user's ID
        $edited_by = auth()->user()->id;

        $order_status = 'Dispatched';

        $order_obfuscator = $request->input('order_id');
        $order = Order::where('obfuscator', $order_obfuscator)->first();;
        // print($order);

        $delivery_person_obfuscator = $request->input('delivery_person');
        // print($delivery_person_obfuscator);exit;

        $delivery_person = DeliveryTeam::where('obfuscator', $delivery_person_obfuscator)->first();
        // print($delivery_person);exit;

        $delivery_person_id = $delivery_person->id;
        // print($delivery_person_id);
        $order_id = $order->id;
        // print($order_id);exit;

        $order->order_status = $order_status;
        $order->edited_by = $edited_by;

        if ($order->save()) {

            $delivery_order = new DeliveryOrder();
            $delivery_order->delivery_person = $delivery_person_id;
            $delivery_order->order_id = $order_id;

            if ($delivery_order->save()) {
                return redirect('orders')->with('status', 'Order is now ready to be dispatched');
            } else {
                return redirect('orders')->with('error', 'Delivery person has not been allocated the order');
            }
        } else {
            return redirect()->back()->with('error', 'Order cannot be dispatched yet');
        }
    }

    public function complete_order(Request $request)
    {

        $this->validate($request, [
            'order_complete' => 'required'
        ]);
        // print($request);exit;

        // Get the user's ID
        $edited_by = auth()->user()->id;

        $order_status = 'Completed';

        $order_obfuscator = $request->input('order_id');
        $order = Order::where('obfuscator', $order_obfuscator)->first();;
        // print($order);exit;

        $order_id = $order->id;
        // print($order_id);exit;

        $order->order_status = $order_status;
        $order->edited_by = $edited_by;

        if ($order->save()) {
            $delivery_status = TRUE;

            $delivery_order = DeliveryOrder::where('order_id', $order_id)->first();
            // print($delivery_order);exit;

            $delivery_order->delivered = $delivery_status;

            if ($delivery_order->save()) {
                return redirect('orders')->with('status', 'Order has been completed and goods delivered successfully');
            } else {
                return redirect('orders')->with('error', 'Error confirming order delivery');
            }
        } else {
            return redirect()->back()->with('error', 'Order has not been completed');
        }
    }
}
