<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;

class CategoryIconsController extends Controller
{

    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    public function add_icon(Request $request){

        // print($request->category);//exit;

        $obfuscator = $request->category;

        $product_category = ProductCategory::where('obfuscator', $obfuscator)->first();
        // print($product_category);exit;

        return view('icons/create')->with('product_category', $product_category);

    }

    public function store_icon(Request $request){

        $this->validate($request, [
            'category_icon' => 'required'
        ]);
        // print($request);

        // Get the user's ID
        $edited_by = auth()->user()->id;

        $obfuscator = $request->input('category');
        // print($obfuscator);exit;

        $product_category = ProductCategory::where('obfuscator', $obfuscator)->first();
        // print($product_category);exit;

        $product_category->icon = $request->input('category_icon');
        $product_category->edited_by = $edited_by;

        if ($product_category->save()) {
            return redirect('product_categories')->with('success', 'Icon has been added');
        } else {
            return redirect('product_categories')->with('error', 'Icon has not been added');
        }

    }

}
