<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Str;

class TestController extends Controller
{

    /**
     * Create a new controller instance
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    public function obfuscate(){

        $obfuscator = Str::random(5);
        print($obfuscator);exit;

    }

}
