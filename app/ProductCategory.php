<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    // Point to table in database
    protected $table = 'product_categories';

    public function sub_categories(){
        return $this->hasMany('App\ProductSubCategory', 'category_id');
    }

    public function brands(){
        return $this->hasMany('App\Brand');
    }

    public function products(){
        return $this->hasMany('App\Product', 'category_id', 'id');
    }
}
