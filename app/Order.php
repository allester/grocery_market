<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // Point to table in database
    protected $table = 'orders';

    public function customer(){
        return $this->belongsTo('App\Customer', 'customer_id', 'id');
    }

    public function products(){
        return $this->belongsToMany('App\Product', 'order_product', 'order_id', 'product_id')->withPivot('quantity', 'total_cost', 'market', 'item_picked');
    }

    public function order_delivery(){
        return $this->hasOne('App\OrderDelivery', 'order_id', 'id');
    }

    public function delivery_person(){
        return $this->belongsToMany('App\DeliveryTeam', 'delivery_order', 'order_id', 'delivery_person')->withPivot('delivered');
    }

    public function market() {
        return $this->belongsTo('App\Market', 'market_id', 'id');
    }
}
