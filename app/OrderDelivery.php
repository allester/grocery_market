<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDelivery extends Model
{
    // Point to table in database
    protected $table = 'order_deliveries';

    public function order(){
        return $this->belongsTo('App\Order', 'order_id', 'id');
    }
}
