<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    // Point to table in database
    protected $table = 'regions';

    public function areas(){
        return $this->hasMany('App\Area');
    }
}
