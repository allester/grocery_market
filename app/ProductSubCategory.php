<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSubCategory extends Model
{
    // Point to table in database
    protected $table = 'sub_categories';

    public function category(){
        return $this->belongsTo('App\ProductCategory', 'category_id');
    }

    public function products(){
        return $this->hasMany('App\Product');
    }
}
