<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Market extends Model
{
    // Point to table in database
    protected $table = 'markets';

    public function vendors()
    {
        return $this->hasMany('App\Vendor', 'vendor_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany('App\Order', 'market_id', 'id');
    }
 
}
