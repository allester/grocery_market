<?php

namespace App;

use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model implements Buyable
{
    // Point to table in database
    protected $table = 'products';

    public function category(){
        return $this->belongsTo('App\ProductCategory', 'category_id', 'id');
    }

    public function brand(){
        return $this->belongsTo('App\Brand');
    }

    // Implement Functions of Buyable Interface

    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    public function getBuyableDescription($options = null)
    {
        return $this->product_name;
    }

    public function getBuyablePrice($options = null)
    {
        return $this->price;
    }

    public function getBuyableWeight($options = null)
    {
        return $this->weight;
    }

    public function orders(){
        return $this->belongsToMany('App\Order', 'order_product', 'product_id', 'order_id');
    }

}
