<?php

namespace App;

use App\Notifications\customerMailResetPasswordNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Database\Eloquent\Model;

class Customer extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attirbutes that are mass assignable.
     *
     * @var array
     */

     protected $fillable = [
        'first_name', 'last_name', 'user_name', 'email', 'obfuscator', 'password',
     ];

     /**
      * The attributes that should be hidden for arrays
      *
      * @var array
      */

      protected $hidden = [
        'password', 'remember_token',
      ];

      /**
       * The attributes that should be cast to native types
       *
       * @var array
       */

       protected $casts = [
            'email_verified_at' => 'datetime',
       ];

      /**
       * Send the password reset notification
       *
       * @param string $token
       * @return void
       */
      public function sendPasswordResetNotification($token){

        $this->notify(new customerMailResetPasswordNotification($token));

      }

      /**
       * Send email verification email
       */
      public function sendEmailVerificationNotification()
      {
          $this->notify(new \App\Notifications\sendCustomerEmailVerificationNotification);
      }

      public function orders(){
          return $this->hasMany('App\Order', 'order_id');
      }
}
