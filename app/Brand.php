<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    // Point to table in database
    protected $table = 'brands';

    public function category(){
        return $this->belongsTo('App\ProductCategory');
    }

    public function products(){
        return $this->hasMany('App\Product');
    }
}
