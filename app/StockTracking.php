<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockTracking extends Model
{
    // Point to table in database
    protected $table = 'stock_tracking';

    public function product(){
        return $this->belongsTo('App\Product');
    }

}
