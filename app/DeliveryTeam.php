<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryTeam extends Model
{
    // Point to table in database
    protected $table = 'delivery_team';

    public function order(){
        return $this->belongsToMany('App\Order', 'delivery_order', 'delivery_person', 'order_id')->withPivot('delivered');
    }

}
