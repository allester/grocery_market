<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEditedByToNullableInProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_categories', function (Blueprint $table) {
            $table->dropForeign('product_categories_edited_by_foreign');
            $table->dropColumn('edited_by');
        });

        Schema::table('product_categories', function (Blueprint $table) {
            $table->bigInteger('edited_by')->unsigned()->nullable()->after('user_id');
            $table->foreign('edited_by')->references('id')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_categories', function (Blueprint $table) {
            $table->dropColumn('edited_by');
        });
    }
}
