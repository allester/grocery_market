<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMarketIdToMarketProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('market_product', function (Blueprint $table) {
            $table->bigInteger('market_id')->unsigned()->after('id');
            $table->foreign('market_id')->references('id')->on('markets')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('market_product', function (Blueprint $table) {
            $table->dropColumn('market_id');
        });
    }
}
