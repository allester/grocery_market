<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePhoneNumberTwoToNullableInDeliveryTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_team', function (Blueprint $table) {
            $table->dropColumn('phone_number_two');
        });

        Schema::table('delivery_team', function (Blueprint $table) {
            $table->string('phone_number_two')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nullable_in_delivery_team', function (Blueprint $table) {
            $table->dropColumn('phone_number_two');
        });
    }
}
