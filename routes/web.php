<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('product_categories', 'ProductCategoriesController');
Route::resource('sub_categories', 'ProductSubCategoriesController');
Route::resource('brands', 'BrandsController');
Route::resource('products', 'ProductsController');
Route::resource('markets', 'MarketsController');
Route::resource('vendors', 'VendorsController');
Route::resource('category', 'CategoryProductsController');

Route::resource('market', 'ShopMarketsController');

Route::get('/fruits', 'CategoryProductsController@show_fruits')->name('fruits');
Route::get('/vegetables', 'CategoryProductsController@show_vegetables')->name('vegetables');
Route::get('/meats', 'CategoryProductsController@show_meats')->name('meats');
Route::get('/potatoes', 'CategoryProductsController@show_potatoes')->name('potatoes');

// Markets
// Route::get('sessions', 'SessionsController');

Route::get('/', 'WelcomeController@index')->name('welcome');
Route::resource('shop', 'ShopController');
Route::get('computers_network_eqpt', 'ShopController@ict_solutions')->name('comps_network_eqpt');
Route::get('power_solutions', 'ShopController@power_solutions')->name('power_solutions');
Route::get('security_solutions', 'ShopController@security_solutions')->name('security_solutions');

// Routes for products per category
Route::get('computers_network_eqpt/{identifier}', 'ShopController@ict_products')->name('comps_network_eqpt_products');
Route::get('power_solutions/{identifier}', 'ShopController@power_products')->name('power_products');
Route::get('security_solutions/{identifier}', 'ShopController@security_products')->name('security_products');

Auth::routes(['verified' => false]);

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::get('customer/sign_up', 'Customer\Auth\RegisterController@customerRegistrationForm')->name('customer.register');
Route::post('customer/register', 'Customer\Auth\RegisterController@create')->name('customer.create');
Route::get('customer/profile', 'CustomerProfileController@profile')->name('customer.profile');
Route::get('customer/profile/edit/{token}', 'CustomerProfileController@edit')->name('customer.profile.edit');
Route::put('customer/profile/update/{token}', 'CustomerProfileController@update')->name('customer.profile.update');

Route::prefix('/customer')->name('customer.')->namespace('Customer')->group(function () {

    // Route::get('/profile', function(){
    //     return view('customer/profile');
    // })->name('profile');


    // All the customer routes will be defined here...
    Route::namespace('Auth')->group(function () {

        // Login Routes
        Route::get('/login', 'LoginController@showLoginForm')->name('login');
        Route::post('/login', 'LoginController@login');
        Route::post('/logout', 'LoginController@logout')->name('logout');

        // Forgot Password Routes
        Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');

        // Reset Password Routes
        Route::get('/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('/password/reset', 'ResetPasswordController@reset')->name('password.update');
    });
});

// Store Front

Route::resource('items', 'ItemsController');

Route::post('cart-add', 'CartController@add')->name('cart.add');
Route::get('cart', 'CartController@cart')->name('cart');
Route::get('cart-clear', 'CartController@clear')->name('cart.clear');
Route::post('cart-increase-qty', 'CartController@cart_increase_qty')->name('cart.increase_qty');
Route::put('cart-update/{token}', 'CartController@update')->name('cart.update');
Route::post('update-cart', 'CartController@update_cart')->name('update.cart');

Route::get('sub_category_related_products', 'AjaxController@get_products_per_sub_category')->name('products_per_sub_category');
Route::get('category_related_sub_categories', 'AjaxController@get_sub_categories_per_category')->name('sub_categories_per_category');
Route::get('areas_per_region', 'AjaxController@areas_per_region')->name('areas_per_region');
Route::post('change_featured_status', 'AjaxController@change_featured_status');
Route::get('sub_categories_per_product_category', 'AjaxController@get_sub_categories_per_product_category')->name('sub_categories_per_product_category');

Route::get('add_icon', 'CategoryIconsController@add_icon')->name('category.add_icon');
Route::post('store_icon', 'CategoryIconsController@store_icon')->name('category.store_icon');

Route::get('successful_order', 'PagesController@order_success')->name('order.success');

// ORDER PROCESSING
Route::get('process_order/{token}', 'OrderDeliveryController@process_order')->name('process_order');
Route::post('process_order_delivery', 'OrderDeliveryController@process_order_delivery')->name('process_order_delivery');
Route::post('dispatch_order', 'OrderDeliveryController@dispatch_order')->name('dispatch_order');
Route::post('complete_order', 'OrderDeliveryController@complete_order')->name('complete_order');

Route::post('check_uncheck_item', 'OrderDeliveryController@mark_unmark_item')->name('check_uncheck_item');

// SEARCH ROUTES
Route::get('search', 'SearchController@search_products')->name('search.products.storefront');
Route::get('autocomplete', 'SearchController@autocomplete')->name('search.autocomplete');

Route::resource('checkout', 'CheckOutController');
Route::resource('regions', 'RegionsController');
Route::resource('areas', 'AreasController');
Route::resource('customer_addresses', 'CustomerAddressesController');
Route::resource('orders', 'OrdersController');
Route::resource('delivery_team', 'DeliveryTeamController');

// Test Area
// Route::get('obfuscate', 'TestController@obfuscate');
