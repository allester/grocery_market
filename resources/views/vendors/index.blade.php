@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Vendors</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <a href="{{ route('vendors.create') }}" class="btn btn-dark btn-block">Add Vendor</a>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-10 offset-md-1">
            @if ($vendors)
                <div class="row">
                    @foreach($vendors as $vendor)
                        <div class="col-md-4">
                            <a href="{{ route('vendors.show', $vendor->obfuscator) }}">
                                <div class="card" >
                                    <img src="{{ $vendor->image_url }}" class="card-img-top" alt="...">
                                    <div class="card-body">
                                      <h5 class="card-title">{{ $vendor->name }}</h5>
                                      <p class="text-dark">{{ $vendor->phone }}</p>
                                      <p class="text-dark text-decoration-none">{{ $vendor->market->market }}</p>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <a href="{{ route('vendors.edit', $vendor->obfuscator) }}" class="btn btn-secondary btn-block">Edit</a>
                                    </div>
                                    <div class="col-md-6">
                                        <form method="POST" action="{{ route('vendors.destroy', $vendor->obfuscator) }}">
                                            @method('DELETE')
                                            {{csrf_field()}}
                                            <button type="submit" class="btn btn-danger btn-block">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-md-4 offset-md-4">
                        {{ $vendors->links() }}
                    </div>
                </div>
            @else
                <p>No vendors to display</p>
            @endif
        </div>
    </div>

</div>

@endsection
