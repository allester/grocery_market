@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Edit Vendor</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <form method="POST" action="{{ route('vendors.update', $vendor->obfuscator) }}">

                @method('PUT')
                {{ csrf_field() }}

                <input type="hidden" name="current_market_id" value="{{ $vendor->market_id }}">

                <div class="form-group">
                    <label for="Market" class="font-weight-bold">Market:</label>
                    <select name="market" id="Market" class="custom-select">
                        <option selected disabled>Change Current Market</option>
                        @if ($markets)
                            @foreach ($markets as $market)
                                <option value="{{ $market->id }}">{{ $market->market }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="form-group">
                    <label for="Market" class="font-weight-bold">Vendor Name:</label>
                    <input type="text" name="vendor_name" class="form-control" value="{{ $vendor->name }}" required />
                </div>

                <div class="form-group">
                    <label for="Email" class="font-weight-bold">Email:</label>
                    <input type="email" name="email" class="form-control" value="{{ $vendor->email }}" />
                </div>

                <div class="form-group">
                    <label for="Market" class="font-weight-bold">Phone:</label>
                    <input type="text" name="phone" class="form-control" value="{{ $vendor->phone }}" required />
                </div>

                <div class="form-group">
                    <label for="imageURL" class="font-weight-bold">Image URL:</label>
                    <input type="text" name="image_url" class="form-control" value="{{ $vendor->image_url }}" required />
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-dark btn-block">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function(){

        var fileLabel = $('.custom-file-label');

        $('.custom-file-input').on('change', function(){
            var fileName = $(this).val();

            fileLabel.text(fileName);
        });

    });

</script>
@endsection
