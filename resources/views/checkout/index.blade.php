@extends('layouts.app3')

@section('content')
<div class="jumbotron bg-primary rounded-0" style="margin-top:-30px;height:250px;" id="welcomeJumbo">
    <div class="row mt-3">
        <div class="col-md-4 text-light">
           <h1>Chakula</h1>
           <h2 class="text-warning">Checkout</h2>
        </div>
    </div>
</div>
    <div class="container">

        <div class="row">
            <div class="col-md-9">
                <h4 class="text-secondary">CHECKOUT</h4>
                <form method="POST" action="{{ route('orders.store') }}">

                    {{ csrf_field() }}

                    {{-- <div class="form-group mt-3">
                        <label for="" class="font-weight-bold text-primary">Choose a market</label>
                        <select name="market" id="Market" class="custom-select">
                            <option selected disabled>Select...</option>
                            @if (isset($markets) && count($markets) > 0)
                                @foreach ($markets as $market)
                                    <option value="{{ $market->id }}">{{ $market->market }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div> --}}

                    <div class="accordion" id="accordionExample">
                        <div class="card border-0 shadow p-3 mb-5 bg-white rounded">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    1. ADDRESS DETAILS
                                </button>
                                </h2>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="firstName" class="font-weight-bold">First Name <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="first_name" value="{{ $customer->first_name }}" id="firstName">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lastName" class="font-weight-bold">Last Name <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="last_name" value="{{ $customer->last_name }}" id="lastName">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="countryCode" class="font-weight-bold">Country Code <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="country_code" value="+256" id="countryCode">
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label for="mobilePhone" class="font-weight-bold">Mobile Phone Number <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="mobile_phone" id="mobilePhone" value="{{ $customer->phone_number }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="billingAddress" class="font-weight-bold">Billing Address <span class="text-danger">*</span></label>

                                            <textarea name="billing_address" id="billingAddress" rows="5" class="form-control" style="margin-top:55px!important;">{{ $customer_address->address }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="Address" class="font-weight-bold">Delivery Address <span class="text-danger">*</span></label>
                                                <div class="mt-3 mb-3">
                                                    <input type="checkbox" name="same_address" id="addressCheck" value="TRUE"> <span class="text-danger font-weight-bold">Delivery Address same as Billing address</span>
                                                </div>
                                                <textarea name="delivery_address" id="Address" rows="5" class="form-control">{{ $customer_address->address }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="Address" class="font-weight-bold">Additional Info</label>
                                                <textarea name="additional_info" id="additionalInfo" rows="5" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="Region" class="font-weight-bold">Region <span class="text-danger">*</span></label>
                                                <select name="region" class="custom-select" id="region">
                                                    <option selected disabled>Select region...</option>
                                                    @if (count($regions) > 0)
                                                        @foreach ($regions as $region)
                                                <option value="{{ $region->obfuscator }}">{{ $region->region }}</option>
                                                        @endforeach
                                                    @else

                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="Area" class="font-weight-bold">District/ Town/ Area <span class="text-danger">*</span></label>
                                                <select name="area" class="custom-select" id="area">

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <p><span class="text-danger">*</span> Required</p>
                                </div>
                            </div>
                        </div>
                        <div class="card border-0 shadow p-3 mb-5 bg-white rounded">
                            <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    2. DELIVERY METHOD
                                </button>
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <h5>How can we deliver your order?</h5>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" name="shipping" class="custom-control-input" value="standard" checked>
                                        <label class="custom-control-label" for="customRadioInline1">Standard Shipping</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card border-0 shadow p-3 mb-5 bg-white rounded">
                            <div class="card-header" id="headingThree">
                                <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    3. PAYMENT METHOD
                                </button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p class="card-text font-weight-bold">Cash on Delivery</p>
                                    <p class="text-secondary">Pay Cash on Delivery</p>
                                    <ul>
                                        <li class="text-secondary">Complete your order and pay cash when you receive your order</li>
                                        <li class="text-secondary">Please make sure to have exact amount on you, since the delivery agent might not have change</li>
                                    </ul>

                                    <div class="row text-right">
                                        <div class="col-md-6">
                                            <p class="card-text text-primary">Subtotal</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="card-text"><span class="currency">UGX</span> <span id="subTotal">{{ Cart::priceTotal(0) }}</span></p>
                                        </div>
                                    </div>
                                    <div class="row text-right">
                                        <div class="col-md-6">
                                            <p class="card-text text-primary">Local Delivey Fee</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="card-text"><span class="currency">UGX</span> <span id="deliveryFee">{{ number_format($delivery_fee) }}</span> </p>
                                        </div>
                                    </div>
                                    <div class="row text-right mt-3">
                                        <div class="col-md-6">
                                            <p class="card-text font-weight-bold text-primary">Total</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="card-text font-weight-bold"><span class="currency">UGX </span><span id="Total">{{ number_format($grand_total) }}</span></p>
                                            <input type="hidden"  name="order_total" value="{{ $grand_total }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-5">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-danger btn-block">CONFIRM ORDER</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-3">
                <h4 class="text-secondary">ORDER SUMMARY</h4>
                <div class="card border-0 shadow  mb-5 bg-white rounded">
                    <div class="card-body">
                        <p>YOUR ORDER (
                            @if (Cart::count() > 1)
                            {{Cart::count()}} items )
                            @else
                                {{Cart::count()}} item )
                            @endif</p>
                            <hr />
                        @if (isset($cart_items) && count($cart_items) > 0)
                            @foreach ($cart_items as $item)
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="card-text text_14">
                                            <span class="font-weight-bold">{{ $item->qty }}</span> {{ $item->name }} - <span class="font-weight-">UGX {{ $item->price }}</span>
                                        </p>

                                    </div>
                                </div>
                            @endforeach
                        @else
                            <p class="text_12">No items in your shopping cart</p>
                        @endif
                        <hr />
                        <div class="row">
                            <div class="col-md-6">
                                <p class="text_14">Subtotal</p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p class="text_14">
                                    <span class="currency">UGX</span> <span id="subTotal2">{{ Cart::priceTotal(0) }}</span>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="text_14">Local Delivery Fee</p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p class="text_14 font-weight-bold"><span class="currency">UGX</span> <span id="deliveryFee2">{{ number_format($delivery_fee) }}</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="text_14 font-weight-bold">Total</p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p class="text_14 text-danger font-weight-bold"><span class="currency">UGX </span><span id="Total2">{{ number_format($grand_total) }}</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <script type="text/javascript">

        let checkBox = document.querySelector('#addressCheck');
        let textArea = document.querySelector('#Address');

        checkBox.addEventListener('click', () => textArea.disabled = checkBox.checked);

        // let subTotal = document.querySelector('#subTotal').innerHTML;
        // // console.log(subTotal);
        // // subTotal = subTotal.replace(",", "");
        // // subTotal = Number(subTotal);

        // let deliveryFee = document.querySelector('#deliveryFee').innerHTML;
        // // console.log(deliveryFee);
        // // deliveryFee = deliveryFee.replace(",", "");
        // // deliveryFee = Number(deliveryFee);

        // let totalFee = parseInt(subTotal) + parseInt(deliveryFee);
        // // console.log(totalFee);

        // // Insert total fee into page
        // let totalField = document.querySelector('#Total');
        // totalField.innerHTML = numberWithCommas(totalFee);

        // let subTotal2 = document.querySelector('#subTotal2').innerHTML;
        // // console.log(subTotal);
        // subTotal2 = subTotal2.replace(",", "");
        // subTotal2 = Number(subTotal2);

        // let deliveryFee2 = document.querySelector('#deliveryFee2').innerHTML;
        // // console.log(deliveryFee);
        // deliveryFee2 = deliveryFee2.replace(",", "");
        // deliveryFee2 = Number(deliveryFee2);

        // let totalFee2 = subTotal2 + deliveryFee2;
        // // console.log(totalFee);

        // // Insert total fee into page
        // let totalField2 = document.querySelector('#Total2');
        // totalField2.innerHTML = numberWithCommas(totalFee2);

        // function numberWithCommas(x){
        //     let parts = x.toString().split(".");
        //     parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //     return parts.join(".");
        // }

        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#region').on('change', function(){
                let selected_region = $('#region option:selected').val();
                let selected_region_2 = $('#region option:selected').text();

                $('#area option').remove();

                // console.log(selected_region);
                // console.log(selected_region_2);

                // Make an ajax request to retrieve areas per region
                $.ajax({
                    method: 'GET',
                    url: '/areas_per_region',
                    data: {region_id: selected_region},
                    dataType: 'JSON',
                    success: function(data){
                        // console.log(data);
                        data.forEach(element => {
                            var area_name = element['area'];
                            var area_id = element['obfuscator'];
                            console.log(area_name + ' ' +area_id)

                            $('#area').append('<option value="'+ element['obfuscator'] +'">'+ element['area'] +'</option>');

                        });
                    },
                    error: function(data){
                        // console.log(data);
                    }
                });

            });

        });

    </script>

@endsection
