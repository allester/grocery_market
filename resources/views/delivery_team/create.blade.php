@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>New Delivery Person</h3>
        </div>
    </div>

    <hr />

    {{-- id 	first_name 	last_name 	picture 	phone_number_one 	phone_number_two 	email 	obfuscator 	identifier 	national_id 	user_id 	edited_by 	created_at 	updated_at
    --}}

    <div class="row">
        <div class="col-md-10 offset-md-1">
            <form method="POST" action="{{ route('delivery_team.store') }}" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="firstName" class="font-weight-bold">First Name*:</label>
                            <input type="text" name="first_name" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="lastName" class="font-weight-bold">Last Name*:</label>
                            <input type="text" name="last_name" class="form-control" required>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        {{-- <div class="col-md-12">
                            <label for="Picture" class="font-weight-bold">Picture*:</label>
                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input input1" id="fileUpload1" name="picture" required>
                                    <label class="custom-file-label label1" for="fileUpload1">Choose File</label>
                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="col-md-6">
                            <label for="nationalID" class="font-weight-bold">National ID:</label>
                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input input2" id="nationalID" name="national_id" required>
                                    <label class="custom-file-label label2" for="nationalID">Choose File</label>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="phoneNumberOne" class="font-weight-bold">Phone Number 1*:</label>
                            <input type="text" name="phone_number_one" class="form-control" required>
                        </div>
                        <div class="col-md-4">
                            <label for="phoneNumberTwo" class="font-weight-bold">Phone Number 2:</label>
                            <input type="text" name="phone_number_two" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="Email" class="font-weight-bold">Email:</label>
                            <input type="text" name="email" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-danger btn-block">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function(){

        let fileLabelOne = $('.label1');
        let fileLabelTwo = $('.label2');

        $('.input1').on('change', function(){
            var fileNameOne = $(this).val();

            fileLabelOne.text(fileNameOne);
        });

        $('.input2').on('change', function(){
            var fileNameTwo = $(this).val();

            fileLabelTwo.text(fileNameTwo);
        });

    });

</script>

@endsection
