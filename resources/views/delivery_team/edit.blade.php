@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Edit Details of Delivery Person</h3>
        </div>
    </div>

    <hr />

    {{-- id 	first_name 	last_name 	picture 	phone_number_one 	phone_number_two 	email 	obfuscator 	identifier 	national_id 	user_id 	edited_by 	created_at 	updated_at
    --}}

    <div class="row">
        <div class="col-md-10 offset-md-1">
            <form method="POST" action="{{ route('delivery_team.update', $delivery_person->obfuscator) }}">

                @method('PUT')
                {{ csrf_field() }}


                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="firstName" class="font-weight-bold">First Name:</label>
                            <input type="text" name="first_name" class="form-control" value="{{ $delivery_person->first_name }}" required>
                        </div>
                        <div class="col-md-6">
                            <label for="lastName" class="font-weight-bold">Last Name:</label>
                            <input type="text" name="last_name" class="form-control" value="{{ $delivery_person->last_name }}" required>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="phoneNumberOne" class="font-weight-bold">Phone Number 1:</label>
                        <input type="text" name="phone_number_one" class="form-control" value="{{ $delivery_person->phone_number_one }}" required>
                        </div>
                        <div class="col-md-4">
                            <label for="phoneNumberTwo" class="font-weight-bold">Phone Number 2:</label>
                            <input type="text" name="phone_number_two" class="form-control" value="{{ $delivery_person->phone_number_two }}">
                        </div>
                        <div class="col-md-4">
                            <label for="Email" class="font-weight-bold">Email:</label>
                            <input type="text" name="email" class="form-control" value="{{ $delivery_person->email }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-danger btn-block">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function(){

        let fileLabelOne = $('.label1');
        let fileLabelTwo = $('.label2');

        $('.input1').on('change', function(){
            var fileNameOne = $(this).val();

            fileLabelOne.text(fileNameOne);
        });

        $('.input2').on('change', function(){
            var fileNameTwo = $(this).val();

            fileLabelTwo.text(fileNameTwo);
        });

    });

</script>

@endsection
