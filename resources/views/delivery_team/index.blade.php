@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Delivery Personnel</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <a href="{{ route('delivery_team.create') }}" class="btn btn-dark btn-block">
                New Delivery Person
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10 offset-md-1">
            @if (count($delivery_personnel) > 0)
                <div class="row">
                    @foreach ($delivery_personnel as $person)
                        <div class="col-md-3 mt-3">
                            <a href="{{ route('delivery_team.show', $person->obfuscator) }}">
                                <div class="card">
                                    <div class="card-body">
                                        {{ $person->first_name }} {{ $person->last_name }}
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="mt-5 mb-5 text-center">
                    <p>No delivery personnel to display</p>
                </div>

            @endif
        </div>
    </div>

</div>

@endsection
