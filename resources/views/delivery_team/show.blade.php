@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Details</h3>
        </div>
    </div>
    <hr />

    {{-- id 	first_name 	last_name 	picture 	phone_number_one 	phone_number_two 	email 	obfuscator 	identifier 	national_id 	user_id 	edited_by 	created_at 	updated_at
    --}}

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card border-0 shadow p-3 mb-5 bg-white rounded">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">First Name:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $delivery_person->first_name }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Last Name:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $delivery_person->last_name }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Picture:</p>
                        </div>
                        <div class="col-md-6">
                            <img src="{{ asset('../storage/uploads/delivery_personnel_picture/'.$delivery_person->picture) }}" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Phone Number 1:</p>
                        </div>
                        <div class="col-md-6">
                        <p class="card-text">{{ $delivery_person->phone_number_one }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Phone Number 2:</p>
                        </div>
                        <div class="col-md-6">
                        <p class="card-text">{{ $delivery_person->phone_number_two }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Email:</p>
                        </div>
                        <div class="col-md-6">
                        <p class="card-text">{{ $delivery_person->email }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-md-6">
                    <a href="{{ route('delivery_team.edit', $delivery_person->obfuscator) }}" class="btn btn-secondary btn-block">Edit</a>
                </div>
                <div class="col-md-6">
                    <form method="POST" action="{{ route('delivery_team.destroy', $delivery_person->obfuscator) }}">
                        @method('DELETE')
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-block">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
