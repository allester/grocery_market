@extends('layouts.app2')

@section('content')
<div class="jumbotron bg-primary rounded-0" style="margin-top:-30px;height:250px;" id="welcomeJumbo">
    <div class="row mt-3">
        <div class="col-md-4 text-light">
           <h1>Chakula</h1>
           <h2 class="text-warning">{{ $title }}</h2>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card border-0 shadow p-3 mb-5 bg-white rounded mt-3">

                <div class="card-body">

                    <form method="POST" action="{{ route($loginRoute) }}">
                        @csrf

                        <div class="form-group mt-3">
                            {{-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> --}}

                            <div class="row">
                                <div class="col-md-8 offset-md-2">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="E-mail Address" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {{-- <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label> --}}
                            <div class="row">
                                <div class="col-md-8 offset-md-2">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8 offset-md-2 text-center">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8 offset-md-2">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        {{ __('Login') }}
                                    </button>


                                </div>
                            </div>
                            <div class="text-center">
                                @if (Route::has('password.request'))
                                <a class="btn btn-link text-danger" href="{{ route($forgotPasswordRoute) }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <p class="mt-2">Don't have an account? <a href="{{ route('customer.register') }}">Sign up</a></p>
            @if (count($brands) > 0)
                <div class="row">
                    @foreach ($brands as $brand)
                        <div class="col-md-2 offset-md-1">
                            <img src="{{ asset('../storage/uploads/brands/'.$brand->brand_logo) }}" alt="" class="img-fluid">
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
