@extends('layouts.app2')

@section('content')
<div class="jumbotron bg-primary rounded-0" style="margin-top:-30px;height:350px;" id="welcomeJumbo">
    <div class="row mt-5">
        <div class="col-md-4 text-light mt-5">
           <h1>Chakula</h1>
        </div>
    </div>
</div>
    <div class="container mb-5">

        <div class="row mt-5 mb-5">
            <div class="col-md-8">
                @if ($products)
                    <div class="row" id="productArea">
                        @foreach ($products as $product)
                            <div class="col-md-4">
                                <a href="{{ route('items.show', $product->obfuscator) }}" class="no-text-decoration">
                                    <div class="card border-0 shadow mb-5 bg-white rounded item-card" style="">
                                        <img src="{{ $product->image_url }}" style="height: 140px;" class="card-img-top" alt="...">
                                        <div class="card-body">
                                          <h5 class="card-title">{{ $product->product_name }}</h5>
                                            <a href="#" class="btn btn-primary mt-3">
                                                <h5 class="card-text text-white">UGX <span>{{ $product->price }}</span></h5>
                                            </a>
                                        </div>
                                    </div>
                                    {{-- <div class="card border-0 shadow p-3 mb-5 bg-white rounded item-card" style="height:300px;">
                                        <div class="card-body border-light">
                                            <img src="{{ $product->image_url }}" class="img-fluid" alt="{{ $product->product_name }}" style="height:100px;">
                                            <p class="card-title mt-3">{{ str_limit($product->product_name, 30) }}</p>
                                        </div>
                                        <div class="card-footer bg-light">
                                            <h5 class="card-text text-dark">UGX <span>{{ $product->price }}</span></h5>
                                        </div>
                                    </div> --}}
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="col-md-4 mb-5" id="sideBanner">
                <img src="https://res.cloudinary.com/dk8qdxlvl/image/upload/e_improve,w_300,h_600,c_thumb,g_auto/v1624911226/ja-ma--gOUx23DNks-unsplash-min_tpvd5w.jpg" alt="">
            </div>
        </div>
    </div>
@endsection
