@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row mt-3 mb-3">
        <div class="col-md-6 offset-md-3">
            <h3>Order Status: <span class="text-danger">{{ $order->order_status }}</span></h3>
            @if ($order->order_status == 'Processed')
                <h5 class="mt-3 mb-3">To be delivered on: <span class="text-danger font-weight-bold">{{ $order->order_delivery->delivery_date }}</span></h5>
            @elseif($order->order_status == 'Completed')
                <h5 class="mt-3 mb-3">Delivery Completed On: <span class="text-danger font-weight-bold">{{ $order->order_delivery->delivery_date }}</span></h5>
            @else
                <p></p>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card border-0 shadow p-3 mb-5 bg-white rounded">
                <div class="card-body">
                    <h4 class="font_weight_600">Order Details</h3>
                    <hr />
                    @foreach ($order->products as $product)
                        <img src="{{ $product->image_url }}" alt="image" class="img-fluid" style="height:200px;">
                        {{-- <p class="card-text">Product: {{ $product->product_name }}</p> --}}
                        <div class="row mt-4">
                            <div class="col-md-4">
                                <p class="card-text font-weight-bold">Product:</p>
                            </div>
                            <div class="col-md-8 text-right">
                                <p>{{ $product->product_name }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <p class="card-text font-weight-bold">Market:</p>
                            </div>
                            <div class="col-md-8 text-right">
                                <p>{{ $product->pivot->market }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="font-weight-bold">Quantity:</p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p>{{ $product->pivot->quantity}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="font-weight-bold">Unit Cost:</p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p>{{ number_format($product->price) }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="font-weight-bold">Total Cost:</p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p>{{ number_format($product->pivot->total_cost) }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="card border-0 shadow p-3 mb-5 bg-white rounded mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Grand Total:</p>
                        </div>
                        <div class="col-md-6 text-right">
                            <p class="text-danger font-weight-bold">UGX {{ number_format($order->order_total) }}</p>
                        </div>
                    </div>
                    <p class="text-secondary text-right">Delivery fee included</p>
                </div>
            </div>
        </div>{{-- end--}}
        <div class="col-md-6">
            <div class="card border-0 shadow p-3 mb-5 bg-white rounded">
                <div class="card-body">
                    <h4 class="font_weight_600">Customer Details</h3>
                    <hr />
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">First Name:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $order->customer->first_name }}</p>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Last Name:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $order->customer->last_name }}</p>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">
                                Phone Number:
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $order->customer->phone_number }}</p>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Email Address:</p>
                        </div>
                        <div class="col-md-6">
                        <p class="card-text">{{ $order->customer->email }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3 mb-5">
                <div class="col-md-12">
                    @if ($order->order_status == 'Pending')
                        <a href="{{ route('process_order', $order->obfuscator) }}" class="btn btn-danger btn-block">Process Order</a>
                    @elseif($order->order_status == 'Processed')
                        <form method="POST" action="{{ route('dispatch_order') }}">

                            {{ csrf_field() }}

                            <input type="hidden" name="order_id" value="{{ $order->obfuscator }}">

                            <div class="form-group">
                                <label for="deliveryTeam" class="font-weight-bold">Choose Delivery Person:</label>
                                <select class="custom-select" id="deliveryTeam" name="delivery_person">
                                    <option selected disabled>Choose...</option>
                                    @if (count($delivery_team) > 0)
                                        @foreach ($delivery_team as $delivery_person)
                                            <option value="{{ $delivery_person->obfuscator }}">{{ $delivery_person->first_name }} {{ $delivery_person->last_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1" name="order_dispatch" value="TRUE" required>
                                <label class="custom-control-label" for="customCheck1">Order is ready for dispatch</label>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-danger btn-block">Dispatch Order</button>
                                </div>
                            </div>

                        </form>
                    @elseif($order->order_status == 'Dispatched')
                        <form method="POST" action="{{ route('complete_order') }}">

                            {{ csrf_field() }}

                            <input type="hidden" name="order_id" value="{{ $order->obfuscator }}">

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1" name="order_complete" value="TRUE" required>
                                <label class="custom-control-label" for="customCheck1">Order is completed</label>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-danger btn-block">Complete Order</button>
                                </div>
                            </div>

                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
