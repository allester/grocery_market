@extends('layouts.app2')

@section('content')
<div class="jumbotron bg-primary rounded-0" style="margin-top:-30px;height:350px;" id="welcomeJumbo">
    <div class="row mt-5">
        <div class="col-md-4 text-light mt-5">
           <h1>Chakula</h1>
           <h2 class="text-warning">Order completed</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    {{-- <form method="GET" action="{{ route('search.products.storefront') }}">
                        <input class="form-control form-control-lg mr-sm-2" type="search" placeholder="Search" name="search" aria-label="Search" id="searchForm">
                    </form> --}}
                </div>
                {{-- <div class="col-md-2">
                    <button type="submit" class="btn btn-light btn-block btn-lg text-primary font-weight-bold"><i class="fas fa-search mr-3"></i></button>
                </div> --}}
            </div>
        </div>
    </div>
</div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 text-center">
                <div class="card border-0 shadow p-3 mt-5 mb-5 bg-white rounded">
                    <div class="card-body">
                        <h3 class="text-warning font-weight-bold">Order Completed Successfully</h3>
                        <hr class="border-primary">
                        <p class="mt-5 mb-5">We have received your order and we are processing it. We will contact you soon. Thank you</p>

                        <div class="row">
                            <div class="col-md-4 offset-md-4 text-center">
                                <a href="{{ url('/') }}"  data-toggle="tooltip" data-placement="bottom" title="Home" class="home-link">
                                    <i class="fas fa-home fa-2x text-primary"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
