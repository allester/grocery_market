@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Orders</h3>
        </div>
    </div>

    <ul class="nav nav-pills nav-fill mt-3 mb-5" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Pending Orders</a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Processed Orders</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="dispatch-tab" data-toggle="tab" href="#dispatch" role="tab" aria-controls="dispatch" aria-selected="false">Dispatched Orders</a>
          </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Completed Orders</a>
        </li>
      </ul>
      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            @if (count($pending_orders) > 0)
                <div class="row">
                    @foreach ($pending_orders as $pending_order)
                        <div class="col-md-3">
                            <a href="{{ route('orders.show', $pending_order->obfuscator) }}" class="no-text-decoration">
                                <div class="card border-0 shadow p-3 mb-5 bg-white rounded">
                                    <div class="card-body">
                                        @foreach ($pending_order->products as $product)
                                            {{-- <img src="{{ $product->image_url }}" alt="image" class="img-fluid" style="height:100px;"> --}}
                                            <p class="text-warning font-weight-bold card-text">{{ $product->product_name }}</p>
                                        @endforeach
                                        <p class="text-dark font-weight-bold">Customer:</p>
                                        <p class="card-text"> {{ $pending_order->customer->first_name }} {{ $pending_order->customer->last_name }}</p>
                                        <hr />
                                        <p class="card-text text-secondary">{{ $pending_order->created_at->diffForHumans() }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="text-center">
                    <p>No orders pending for now</p>
                </div>
            @endif
        </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            @if (count($processed_orders) > 0)
                <div class="row">
                    @foreach ($processed_orders as $processed_order)
                        <div class="col-md-3">
                            <a href="{{ route('orders.show', $processed_order->obfuscator) }}" class="no-text-decoration">
                                <div class="card border-0 shadow p-3 mb-5 bg-white rounded">
                                    <div class="card-body">
                                        @foreach ($processed_order->products as $product)
                                            {{-- <img src="{{ $product->image_url }}" alt="image" class="img-fluid" style="height:100px;"> --}}
                                            <p class="text-warning font-weight-bold card-text">{{ $product->product_name }}</p>
                                        @endforeach
                                        <p class="text-dark font-weight-bold">Customer:</p>
                                        <p class="card-text"> {{ $processed_order->customer->first_name }} {{ $processed_order->customer->last_name }}</p>
                                        <hr />
                                        <p class="card-text text-secondary">{{ $processed_order->created_at->diffForHumans() }}</p>
                                        <hr />
                                        <p class="card-text"><span class="text-danger font-weight-bold">Delivery Date:</span><br /><span>{{ $processed_order->order_delivery->delivery_date }}</span></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="text-center">
                    <p>No orders awaiting dispatch</p>
                </div>
            @endif
        </div>
        <div class="tab-pane fade" id="dispatch" role="tabpanel" aria-labelledby="dispatch-tab">
            @if (count($dispatched_orders) > 0)
                <div class="row">
                    @foreach ($dispatched_orders as $dispatched_order)
                        <div class="col-md-3">
                            <a href="{{ route('orders.show', $dispatched_order->obfuscator) }}" class="no-text-decoration">
                                <div class="card border-0 shadow p-3 mb-5 bg-white rounded">
                                    <div class="card-body">
                                        @foreach ($dispatched_order->products as $product)
                                            {{-- <img src="{{ $product->image_url }}" alt="image" class="img-fluid" style="height:100px;"> --}}
                                            <p class="text-warning font-weight-bold card-text">{{ $product->product_name }}</p>
                                        @endforeach
                                        <p class="text-dark font-weight-bold">Customer:</p>
                                        <p class="card-text"> {{ $dispatched_order->customer->first_name }} {{ $dispatched_order->customer->last_name }}</p>
                                        <hr />
                                        <p class="card-text text-secondary">{{ $dispatched_order->created_at->diffForHumans() }}</p>
                                        <hr />
                                        <p class="card-text"><span class="text-danger font-weight-bold">Delivery Date:</span><br /><span>{{ $dispatched_order->order_delivery->delivery_date }}</span></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="text-center">
                    <p>No dispatched orders to display</p>
                </div>
            @endif
        </div>
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
            @if (count($completed_orders))
                <div class="row">
                    @foreach ($completed_orders as $completed_order)
                        <div class="col-md-3">
                            <a href="{{ route('orders.show', $completed_order->obfuscator) }}" class="no-text-decoration">
                                <div class="card border-0 shadow p-3 mb-5 bg-white rounded">
                                    <div class="card-body">
                                        @foreach ($completed_order->products as $product)
                                            {{-- <img src="{{ $product->image_url }}" alt="image" class="img-fluid" style="height:100px;"> --}}
                                            <p class="text-warning font-weight-bold card-text">{{ $product->product_name }}</p>
                                        @endforeach
                                        <p class="text-dark font-weight-bold">Customer:</p>
                                        <p class="card-text"> {{ $completed_order->customer->first_name }} {{ $completed_order->customer->last_name }}</p>
                                        <hr />
                                        <p class="card-text text-secondary">{{ $completed_order->created_at->diffForHumans() }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="text-center">
                    <p>No completed orders to display</p>
                </div>
            @endif
        </div>
    </div>
</div>

@endsection
