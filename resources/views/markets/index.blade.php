@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Markets</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <a href="{{ route('markets.create') }}" class="btn btn-dark btn-block">Add Market</a>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-10 offset-md-1">
            @if ($markets)
                <div class="row">
                    @foreach($markets as $market)
                        <div class="col-md-4">
                            <div class="card" style="">
                                <img src="{{ $market->image_url }}" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $market->market }}</h5>
                                    {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-6">
                                    <a href="{{ route('markets.edit', $market->obfuscator) }}" class="btn btn-secondary btn-block">Edit</a>
                                </div>
                                <div class="col-md-6">
                                    <form method="POST" action="{{ route('markets.destroy', $market->obfuscator) }}">
                                        @method('DELETE')
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-danger btn-block">Delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-md-4 offset-md-4">
                        {{ $markets->links() }}
                    </div>
                </div>
            @else
                <p>No markets to display</p>
            @endif
        </div>
    </div>

</div>

@endsection
