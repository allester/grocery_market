@extends('layouts.app2')

@section('content')

<div class="jumbotron bg-primary rounded-0" style="margin-top:-30px;height:200px;" id="welcomeJumbo">
    <div class="row">
        <div class="col-md-4 text-light mt-5">
           <h1>{{ $market->market }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="row">
            </div>
        </div>
    </div>
</div>

<div class="container">
    @if (isset($categories) && count($categories) > 0)
        @foreach ($categories as $category)
            <h2 class="text-warning mt-5 mb-5">{{ $category->category }}</h2>
            @if (count($category->products) > 0)
                <div class="row d-flex flex-row flex-nowrap overflow-auto">
                    @foreach ($category->products as $product)
                        <div class="col-md-3" style="margin-left:-20px">
                            <a href="{{ route('items.show',['item' => $product->obfuscator, 'market' => $market->obfuscator]) }}" class="no-text-decoration">
                                <div class="card border-0 shadow mb-5 bg-white rounded item-card" style="">
                                    <img src="{{ $product->image_url }}" style="height: 140px;" class="card-img-top" alt="...">
                                    <div class="card-body">
                                      <h5 class="card-title">{{ $product->product_name }}</h5>
                                        <a href="#" class="btn btn-primary mt-3">
                                            <h5 class="card-text text-white">UGX <span>{{ $product->price }}</span></h5>
                                        </a>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @endif
        @endforeach
    @endif
</div>

@endsection
