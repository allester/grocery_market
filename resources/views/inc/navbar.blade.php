<nav class="navbar navbar-dark sticky-top bg-light flex-md-nowrap p-0 shadow">
    <a class="navbar-brand text-warning col-md-3 col-lg-2 mr-0 px-3" href="{{ route('dashboard') }}" style="font-family: 'Comfortaa', cursive !important;">
        <h3>Chakula</h3>
    </a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation" style="margin-left:350px;">
      <span class="navbar-toggler-icon"></span>
    </button>

  </nav>

  <div class="container-fluid">
    <div class="row">
      <nav id="sidebarMenu" class="col-md-3 sticky-top col-lg-2 d-md-block bg-secondary sidebar collapse" style="height: 100vh!important;">
        <div class="sidebar-sticky pt-3">
          <ul class="nav flex-column">
            @if (Auth::guard('web')->check())
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::guard('web')->user()->name }} (ADMIN) <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a href="" class="dropdown-item">Profile</a>
                        <a class="dropdown-item" href="#" onclick="event.preventDefault();document.querySelector('#logout-form').submit();">
                            Logout
                        </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
                        @csrf
                    </form>
                    </div>
                </li>
            @endif
            <li class="nav-item">
              <a class="nav-link active text-warning" href="{{ route('dashboard') }}">
                <span data-feather="home"></span>
                Dashboard <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-warning" href="{{ route('markets.index') }}">
                    Markets
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-warning" href="{{ route('vendors.index') }}">
                    Vendors
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-warning" href="{{ route('orders.index') }}">
                    Orders
                </a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-warning" href="{{ route('product_categories.index') }}">

                Product Categories
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-warning" href="{{ route('sub_categories.index') }}">

                Sub Categories
              </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-warning" href="{{ route('brands.index') }}">

                    Brands
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-warning" href="{{ route('products.index') }}">

                    Products
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-warning" href="{{ route('regions.index') }}">

                    Regions
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-warning" href="{{ route('areas.index') }}">

                    Areas
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-warning" href="{{ route('delivery_team.index') }}">
                    Delivery Team
                </a>
            </li>
          </ul>

        </div>
      </nav>

      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
{{-- ============================================ --}}

