<footer class="bg-dark mt-5" style="position:relative; bottom:0; height: 260px;">
    <div class="container mt-3">
        <div class="row mt-3">
            <div class="col-md-4 mt-5">
                {{-- <h3 class="text-warning">
                    <i class="fas fa-drumstick-bite fa-3x text-warning"></i>
                </h3> --}}
                <h3 class="text-white">Chakula</h3>
                <p class="text-warning">Groceries and fresh produce delivered to your door step</p>
            </div>
            <div class="col-md-4 mt-5">
                <a href="" class="text-white">
                    <p>Customer Login</p>
                </a>
                <a href="" class="text-white">
                    <p>Terms of Use</p>
                </a>
                <a href="" class="text-white">
                    <p>Privacy Policy</p>
                </a>
            </div>
            <div class="col-md-4 mt-5">
                <p class="text-white"><i class="fas fa-phone-square-alt mr-3"></i> +256 700123456</p>
                <p class="text-white"><i class="fas fa-envelope mr-3"></i> info@example.com</p>
            </div>
        </div>
        <hr style="border border-white" />
        <p class="text-white text-center font-weight-bold" style="bottom:20px">
            &copy; 2021 Chakula. All rights reserved
        </p>
    </div>
</footer>
