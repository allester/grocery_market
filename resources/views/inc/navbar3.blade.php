<nav class="navbar navbar-expand-md navbar-light bg-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand text-warning font_weight_600 mr-5" href="{{ url('/') }}" style="font-family: 'Comfortaa', cursive !important;">
            <h3>Chakula</h3>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-5">
                <li class="nav-item">
                    <a class="nav-link text-primary text_16 font-weight-bold" href="{{ route('welcome') }}" style="font-family: 'Comfortaa', cursive !important;">Markets</a>
                </li>
            </ul>
            {{-- Right side of Navbar --}}
            <ul class="navbar-nav ml-auto">
                {{-- Authentication Links --}}
                {{-- @if (Auth::guard('web')->check())
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::guard('web')->user()->name }} (ADMIN) <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a href="{{ route('dashboard') }}" class="dropdown-item">Dashboard</a>
                            <a class="dropdown-item" href="#" onclick="event.preventDefault();document.querySelector('#logout-form').submit();">
                                Logout
                            </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
                            @csrf
                        </form>
                        </div>
                    </li>
                @endif --}}
                @if (Auth::guard('customer')->check())
                    <li class="nav-item dropdown">
                        <a id="customerDropdown" class="nav-link dropdown-toggle text_16 font-weight-bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="font-family: 'Expletus Sans', cursive !important;">
                            {{ Auth::guard('customer')->user()->first_name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="customerDropdown">
                            <a href="{{ route('customer.profile') }}" class="dropdown-item" style="font-family: 'Expletus Sans', cursive !important;">Profile</a>
                            <a class="dropdown-item" href="#" onclick="event.preventDefault();document.querySelector('#customer-logout-form').submit();" style="font-family: 'Expletus Sans', cursive !important;">
                                Logout
                            </a>
                            <form id="customer-logout-form" action="{{ route('customer.logout') }}" method="POST" style="display:none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link text-primary text_16 font-weight-bold" href="{{ route('customer.login') }}" style="font-family: 'Expletus Sans', cursive !important;">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-primary text_16 font-weight-bold" href="{{ route('customer.register') }}" style="font-family: 'Expletus Sans', cursive !important;">Register</a>
                    </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link text-primary text_16 font-weight-bold" href="{{ route('cart') }}" style="font-family: 'Expletus Sans', cursive !important;"><i class="fas fa-shopping-cart text-warning"></i><span class="badge badge-pill badge-danger">{{ Cart::count() }}</span> Cart</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
