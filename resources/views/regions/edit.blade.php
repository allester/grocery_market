@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Edit Region</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <form method="POST" action="{{ route('regions.update', $region->obfuscator) }}">

                @method('PUT')
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Region">Region:</label>
                        <input type="text" name="region" class="form-control" value="{{ $region->region }}" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-dark btn-block">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

@endsection
