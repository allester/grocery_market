@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Regions</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <a href="{{ route('regions.create') }}" class="btn btn-dark btn-block">Add Region</a>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-10 offset-md-1">
            @if (count($regions) > 0)
                <div class="row">
                    @foreach ($regions as $region)
                        <div class="col-md-3 mt-3">
                            <a href="{{ route('regions.show', $region->obfuscator) }}">
                                <div class="card">
                                    <div class="card-body">
                                        {{ $region->region }}
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <p>No regions to display</p>
            @endif
        </div>
    </div>

</div>

@endsection
