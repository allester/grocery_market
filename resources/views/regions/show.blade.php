@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Region Details</h3>
        </div>
    </div>

    <hr />

    {{-- id 	region 	obfuscator 	user_id 	edited_by 	created_at 	updated_at  --}}

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text">Region:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $region->region }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-6">
                    <a href="{{ route('regions.edit', $region->obfuscator) }}" class="btn btn-secondary btn-block">Edit</a>
                </div>
                <div class="col-md-6">
                    <form method="POST" action="{{ route('regions.destroy', $region->obfuscator) }}">
                        @method('DELETE')
                        {{ csrf_field() }}
                        @if ($region->validity == TRUE)
                            <button type="submit" class="btn btn-danger btn-block">Disable</button>
                        @else
                            <button type="submit" class="btn btn-success btn-block">Enable</button>
                        @endif

                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
