@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Areas</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <a href="{{ route('areas.create') }}" class="btn btn-dark btn-block">Add Area</a>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-10 offset-md-1">
            @if (count($areas) > 0)
                <div class="row">
                    @foreach ($areas as $area)
                        <div class="col-md-3 mt-3">
                            <a href="{{ route('areas.show', $area->obfuscator) }}">
                                <div class="card">
                                    <div class="card-body">
                                        {{ $area->area }}
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="row mt-3 mb-3">
                    <div class="col-md-4 offset-md-4 text-center">
                        {{ $areas->links() }}
                    </div>
                </div>
            @else
                <p>No area to display</p>
            @endif
        </div>
    </div>

</div>

@endsection
