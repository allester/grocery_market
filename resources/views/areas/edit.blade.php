@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Edit Area</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <form method="POST" action="{{ route('areas.update', $area->obfuscator) }}">

                @method('PUT')
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Region">Region</label>
                            <select name="region" class="custom-select" id="Region">
                                @if (count($regions) > 0)
                            <option selected value="{{ $current_region->id }}">{{ $current_region->region }} - Current</option>
                            <option disabled>Change region...</option>
                                    @foreach ($regions as $region)
                                        <option value="{{ $region->id }}">{{ $region->region }}</option>
                                    @endforeach
                                @else
                                    <option>No region to display</option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="Area">Area</label>
                            <input type="text" class="form-control" name="area" id="Area" value="{{ $area->area }}" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-dark btn-block">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

@endsection
