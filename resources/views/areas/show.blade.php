@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Area Details</h3>
        </div>
    </div>

    <hr />

    {{-- id 	area 	obfuscator 	region_id 	user_id 	edited_by 	created_at 	updated_at --}}

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text">Area:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $area->area }}</p>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <p class="card-text">Region:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $region->region }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-6 mt-3">
                    <a href="{{ route('areas.edit', $area->obfuscator) }}" class="btn btn-secondary btn-block">Edit</a>
                </div>
                <div class="col-md-6 mt-3">
                    <form method="POST" action="{{ route('areas.destroy', $area->obfuscator) }}">
                        @method('DELETE')
                        {{ csrf_field() }}
                        @if ($area->validity == TRUE)
                            <button type="submit" class="btn btn-danger btn-block">Disable</button>
                        @else
                            <button type="submit" class="btn btn-success btn-block">Enable</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
