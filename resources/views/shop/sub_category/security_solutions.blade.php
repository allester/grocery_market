@extends('layouts.app2')

@section('content')

<div class="container">
    <div class="jumbotron bg-primary rounded-0 text-center" style="margin-top:-30px;height:350px;" id="securityJumbo">
        <h1 class="text-white mt-5 pt-5 pb-5 bg-dark">Security Solutions</h1>
        <h3 class="text-warning bg-white font-weight-bold pt-3 pb-3">{{ $sub_category->sub_category }}</h3>
    </div>
    @if (count($brands) > 0)
        <div class="row">
            @foreach ($brands as $brand)
                <div class="col-md-2">
                    <img src="{{ asset('../storage/uploads/brands/'.$brand->brand_logo) }}" alt="" class="img-fluid">
                </div>
            @endforeach
        </div>
    @endif

    <div class="row mt-5 mb-5">
        <div class="col-md-2">
            <div class="list-group">
                @if (count($five_sub_categories) > 0)
                    @foreach ($five_sub_categories as $sub_category)
                        <a href="{{ route('security_products', $sub_category->obfuscator) }}" class="list-group-item list-group-item-action text-primary">{{ $sub_category->sub_category }}</a>
                    @endforeach
                    <a type="button" class="list-group-item list-group-item-action active mt-3" data-toggle="modal" data-target="#subsModal">More</a>
                @endif
            </div>
        </div>
        <div class="col-md-8">
            {{-- Display products here --}}
            @if ($products)
                <div class="row">
                    @foreach ($products as $product)
                        <div class="col-md-4">
                            <a href="{{ route('items.show', $product->obfuscator) }}" class="no-text-decoration">
                                <div class="card border-0 shadow p-3 mb-5 bg-white rounded item-card" style="height:300px;">
                                    <div class="card-body border-light">
                                        <img src="{{ asset('storage/uploads/products/'.$product->product_image) }}" alt="{{ $product->product_name }}" style="height:100px;" class="img-fluid" />
                                        <p class="card-title mt-3">{{ str_limit($product->product_name, 30) }}</p>
                                    </div>
                                    <div class="card-footer bg-light">
                                        <h5 class="card-text text-dark">UGX <span>{{ $product->price }}</span></h5>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-md-4 offset-md-4">
                        {{ $products->links() }}
                    </div>
                </div>
            @endif
        </div>
    </div>

    {{-- Sub Categories Modal --}}
    <div class="modal fade" id="subsModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title text-primary" id="exampleModalLabel">Security Solutions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    @if (count($sub_categories) > 0)
                        <div class="row">
                            @foreach ($sub_categories as $sub_category)
                                <div class="col-md-3 mt-3">
                                    <a href="{{ route('security_products', $sub_category->obfuscator) }}">
                                        <div class="card border-0 shadow p-3 bg-white rounded">
                                            <div class="card-body">
                                                {{ $sub_category->sub_category }}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
