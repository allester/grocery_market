@extends('layouts.app2')

@section('content')
<div class="jumbotron bg-primary rounded-0" style="margin-top:-30px;height:250px;" id="welcomeJumbo">
    <div class="row mt-3">
        <div class="col-md-4 text-light">
           <h1>Chakula</h1>
           <h2 class="text-warning">My Profile</h2>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h4 class="text-center">{{ __('Edit Billing Address') }}</h4>

            <form method="POST" action="{{ route('customer_addresses.update', $billing_address->id) }}">

                @method('PUT')
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="billingAddress" class="font-weight-bold">Billing Address</label>
                    <textarea name="billing_address" rows="5" class="form-control">{{ $billing_address->address }}</textarea>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection
