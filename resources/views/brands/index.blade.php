@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Brands</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <a href="{{ route('brands.create') }}" class="btn btn-dark btn-block">Add Brand</a>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-10 offset-md-1">
            @if ($brands)
                <div class="row">
                    @foreach ($brands as $brand)
                        <div class="col-md-3 mt-3">
                            <a href="{{ route('brands.show', $brand->obfuscator) }}">
                                <div class="card">
                                    <div class="card-body">
                                        {{ $brand->brand }}
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-4 offset-md-4 text-center">
            {{ $brands->links() }}
        </div>
    </div>

</div>

@endsection
