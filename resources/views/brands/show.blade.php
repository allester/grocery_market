@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Brand Details</h3>
        </div>
    </div>

    <hr />

    {{--  id 	brand 	brand_logo 	category_id 	user_id 	edited_by 	created_at 	updated_at  --}}
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-body">
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <p class="card-text">Brand:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $brand->brand }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text">Brand Logo:</p>
                        </div>
                        <div class="col-md-6">
                        <img src="{{ asset('../storage/uploads/brands/'.$brand->brand_logo) }}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text">Category:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $category->category }}</p>
                        </div>
                    </div>
                </div>
            </div>{{-- end of .card --}}
            <div class="row mt-3">
                <div class="col-md-6">
                    <a href="{{ route('brands.edit', $brand->obfuscator) }}" class="btn btn-secondary btn-block">Edit</a>
                </div>
                <div class="col-md-6">
                    <form method="POST" action="{{ route('brands.destroy', $brand->obfuscator) }}">
                        @method('DELETE')
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger btn-block">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
