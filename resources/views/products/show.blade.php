@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Product Details</h3>
        </div>
    </div>

    <hr />

    {{-- id 	product_name 	product_image 	price 	track_stock 	description 	category_id 	user_id 	edited_by 	stock_tracking_id 	brand_id 	obfuscator 	created_at 	updated_at  --}}

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="row mt-3 mb-3">
                <div class="col-md-6">
                    <a href="{{ route('products.edit', $product->obfuscator) }}" class="btn btn-secondary btn-block">Edit</a>
                </div>
                <div class="col-md-6">
                    <form method="POST" action="{{ route('products.destroy', $product->obfuscator) }}">
                        @method('DELETE')
                        {{ csrf_field() }}
                        @if ($product->available == TRUE)
                            <button type="submit" class="btn btn-danger btn-block">Change Status to Not Available</button>
                        @else
                            <button type="submit" class="btn btn-danger btn-block">Change Status to Available</button>
                        @endif
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Product:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $product->product_name }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Product Image:</p>
                        </div>
                        <div class="col-md-6">
                            <img src="{{ asset('../storage/uploads/products/'.$product->product_image) }}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Price:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">UGX {{ $product->price }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Stock Tracking:</p>
                        </div>
                        <div class="col-md-6">
                            @if ($product->track_stock == TRUE)
                                <p class="card-text">Yes</p>
                            @else
                                <p class="card-text">No</p>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="card-text font-weight-bold">Category:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $category->category }}</p>
                        </div>
                    </div>
                    @if ($product->brand !== NULL || $product->brand !== '')
                        <div class="row">
                            <div class="col-md-6">
                                <p class="card-text font-weight-bold">Brand:</p>
                            </div>
                            <div class="col-md-6">
                                <p class="card-text">{{ $product->brand->brand }}</p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>{{-- end of .card --}}
            <div class="row mt-3">
                <div class="col-md-12">
                    <label for="Description" class="font-weight-bold">Description:</label>
                    {!! $product->description !!}
                </div>
            </div>

        </div>
    </div>

</div>

@endsection
