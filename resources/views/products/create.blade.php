@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Add Product</h3>
        </div>
    </div>

    <hr />

    {{-- id 	product_name 	product_image 	price 	track_stock 	description 	category_id 	user_id 	edited_by 	stock_tracking_id 	brand_id 	created_at 	updated_at  --}}

    <div class="row">
        <div class="col-md-10 offset-md-1">
            <form method="POST" action="{{ route('products.store') }}" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="productName">Product Name:</label>
                            <input type="text" name="product_name" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="productImage">Product Image:</label>
                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="fileUpload" name="picture">
                                    <label class="custom-file-label" for="fileUpload">Choose File</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Image URL:</label>
                            <input type="text" name="image_url" class="form-control" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="Price">Price:</label>
                            <input type="text" name="price" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="productCategory">Product Category:</label>
                            <select name="product_category" class="custom-select" id="productCategory">
                                <option selected disabled>Choose...</option>
                                @if ($product_categories)
                                    @foreach ($product_categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->category }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        {{-- <div class="col-md-4">
                            <label for="Brand">Brand:</label>
                            <select name="brand" class="custom-select">
                                <option selected disabled>Choose...</option>
                                @if ($brands)
                                    @foreach($brands as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->brand }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div> --}}
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="subCategory">Sub Category</label>
                            <select name="sub_category" class="custom-select" id="subCategory">
                                <option selected disabled>Choose...</option>
                                @if ($sub_categories)
                                    @foreach ($sub_categories as $sub_category)
                                        <option value="{{ $sub_category->id }}">{{ $sub_category->sub_category }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        {{-- <div class="col-md-6">
                            <label for="trackStock">Track Stock?</label>
                            <select name="track_stock" class="custom-select">
                                <option disabled selected>Choose...</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="Stock">Initial Stock</label>
                            <input type="text" name="initial_stock" class="form-control">
                        </div> --}}
                    </div>
                </div>

                {{-- <div class="form-group">
                    <label for="productDescription">Product Description:</label>
                    <textarea name="product_description" class="form-control" id="editor1" rows="5"></textarea>
                </div> --}}

                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-dark btn-block">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#productCategory').on('change', function(){

            let selected_category = $('#productCategory option:selected').val();
            // console.log(selected_category);

            $('#subCategory option').remove();

            $.ajax({
                method: 'GET',
                url: '/sub_categories_per_product_category',
                data: {category_id: selected_category},
                dataType: 'JSON',
                success: function(data){
                    // console.log(data);
                    data.forEach(element => {
                        var sub_category_name = element['sub_category'];
                        var sub_category_id = element['id'];

                        $('#subCategory').append('<option value="'+ sub_category_id +'">' + sub_category_name + '</option>');
                    });
                },
                error: function(data){
                    // console.log(data);
                }
            });

        });

        var fileLabel = $('.custom-file-label');

        $('.custom-file-input').on('change', function(){
            var fileName = $(this).val();

            fileLabel.text(fileName);
        });

    });

</script>

@endsection
