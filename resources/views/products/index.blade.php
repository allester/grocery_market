@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="jumbotron bg-primary rounded-0" style="margin-top:-30px;height:250px;" id="welcomeJumbo">
        <div class="row mt-5">
            <div class="col-md-4 text-light mt-5">
                <h3>Products</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">

        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <a href="{{ route('products.create') }}" class="btn btn-dark btn-block">
                Add Product
            </a>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-10 offset-md-1">
            @if ($products)
                <div class="row">
                    @foreach ($products as $product)
                        <div class="col-md-3 mt-3">
                            <a href="{{ route('products.show', $product->obfuscator) }}">
                                <div class="card border-0 shadow p-3 mb-5 bg-white rounded" style="">
                                    <img src="{{ $product->image_url }}" class="card-img-top" alt="...">
                                    <div class="card-body">
                                      <h5 class="card-title">{{ $product->product_name }}</h5>
                                      {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <p>No products to display</p>
            @endif
        </div>
    </div>

</div>

@endsection
