@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Edit Product</h3>
        </div>
    </div>

    <hr />

    {{-- id 	product_name 	product_image 	price 	track_stock 	description 	category_id 	user_id 	edited_by 	stock_tracking_id 	brand_id 	created_at 	updated_at  --}}

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <form method="POST" action="{{ route('products.update', $product->obfuscator) }}" enctype="multipart/form-data">
                @method('PUT')
                {{ csrf_field() }}

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="productName">Product Name:</label>
                            <input type="text" name="product_name" class="form-control" value="{{ $product->product_name }}">
                        </div>
                        <div class="col-md-6">
                            <label for="productImage">Product Image:</label>
                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="fileUpload" name="picture">
                                    <label class="custom-file-label" for="fileUpload">Choose File</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Image URL:</label>
                            <input type="text" name="image_url" class="form-control" value="{{ $product->image_url }}" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="Price">Price:</label>
                            <input type="text" name="price" class="form-control" value="{{ $product->price }}">
                        </div>
                        <div class="col-md-4">
                            <label for="productCategory">Product Category:</label>
                            <select name="product_category" class="custom-select">
                                <option value="{{ $product_category->id }}" selected>{{ $product_category->category }} - Current</option>
                                @if ($categories)
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->category }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        {{-- <div class="col-md-4">
                            <label for="Brand">Brand:</label>
                            <select name="brand" class="custom-select">
                                <option value="{{ $product_brand->id }}" selected>{{ $product_brand->brand }} - Current</option>
                                @if ($brands)
                                    @foreach($brands as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->brand }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div> --}}
                    </div>
                </div>

                {{-- <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="trackStock">Track Stock?</label>
                            <select name="track_stock" class="custom-select">
                                <option disabled selected>Choose...</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="Stock">Initial Stock</label>
                            <input type="text" name="initial_stock" class="form-control" value="">
                        </div>
                    </div>
                </div> --}}

                <div class="form-group">
                    <label for="productDescription">Product Description:</label>
                    <textarea name="product_description" class="form-control" rows="5">{{ $product->description }}</textarea>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-dark btn-block">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function(){

        var fileLabel = $('.custom-file-label');

        $('.custom-file-input').on('change', function(){
            var fileName = $(this).val();

            fileLabel.text(fileName);
        });

    });

</script>

@endsection
