@extends('layouts.app3')


@section('content')
    <div class="jumbotron bg-primary rounded-0" style="margin-top:-30px;height:250px;" id="welcomeJumbo">
        <div class="row mt-3">
            <div class="col-md-4 text-light">
            <h1>Chakula</h1>
            <h2 class="text-warning">Shopping Cart</h2>
            </div>
        </div>
    </div>
    <div class="container">

        {{-- <div class="row mt-5 mb-5">
            <div class="col-md-6 offset-md-3">
                <form method="GET" action="{{ route('search.products.storefront') }}">
                    <input class="form-control form-control-lg mr-sm-2" type="search" placeholder="Search" name="search" aria-label="Search" id="searchForm">
                </form>
            </div>
        </div> --}}

        <h3 class="mt-3">Cart (
            @if (Cart::count() > 1)
            {{Cart::count()}} items )
            @else
                {{Cart::count()}} item )
            @endif</h3>
        <div class="row mt-3" id="cartHeadings">
            <div class="col-md-2">
                <h5 class="text-secondary font-weight-bold">ITEM</h5>
            </div>
            <div class="col-md-2">

            </div>
            <div class="col-md-2">
                <h5 class="text-secondary font-weight-bold">QUANTITY</h5>
            </div>
            <div class="col-md-3">
                <h5 class="text-secondary font-weight-bold">UNIT PRICE</h5>
            </div>
            <div class="col-md-3">
                <h5 class="text-secondary font-weight-bold">SUB-TOTAL</h5>
            </div>
        </div>
        @if (isset($cart_items) && count($cart_items) > 0)
            @foreach ($cart_items as $item)
                <div class="card border-0 shadow p-3 mb-5 bg-white rounded mt-5">
                    <div class="card-body">
                        <form method="POST" action="{{ route('cart.update', $item->rowId) }}">
                            @method('PUT')
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-2">
                                    {{-- Image --}}
                                    <img src="{{ $item->options->image }}" alt="image" class="img-fluid" id="imgCart">
                                </div>
                                <div class="col-md-2">
                                    {{-- Item name --}}
                                    <p class="card-text"><span class="font-weight-bold hidden_cart_title mr-2">Item: </span>{{ $item->name }}</p>
                                    <p class="card-text hidden">{{ $item->rowId }}</p>
                                </div>
                                <div class="col-md-2">
                                    {{-- Quantity --}}
                                    <div class="form-group">
                                        <span class="hidden_cart_title font-weight-bold mr-2">Quantity: </span>
                                        <input type="number" name="quantity" id="qty" min="1" value="{{$item->qty}}" class="form-control {{$item->rowId}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    {{-- Unit Price --}}
                                    <p class="card-text"><span class="hidden_cart_title font-weight-bold mr-2">Unit Price: </span>UGX {{ number_format($item->price) }}</p>
                                </div>
                                <div class="col-md-3">
                                    {{-- Sub-Total --}}
                                    <p class="card-text sub_total" id="subTotal">
                                        <span class="hidden_cart_title font-weight-bold mr-2">Sub-Total: </span>
                                        UGX {{ number_format($item->subtotal) }}</p>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endforeach
        @else
            <p>No items in your shopping cart</p>
        @endif


        <div class="row">
            <div class="col-md-2 offset-md-6 text-right">
                <p class="font-weight-bold">Total</p>
            </div>
            <div class="col-md-4 text-right">
                <h4 class="text-danger font-weight-bold" id="priceTotal">UGX <span id="priceTotalValue">{{ Cart::priceTotal(0) }}</span></h4>
                <p class="text-secondary">Does not include local delivery fees</p>
            </div>
        </div>

        <div class="card border-0 shadow p-3 mb-5 bg-white rounded">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 offset-md-3 mt-3">
                        <a href="{{ route('cart.clear') }}" class="btn btn-warning btn-block font-weight-bold">CLEAR CART</a>
                    </div>
                    <div class="col-md-3 mt-3">
                        <a href="{{ route('welcome') }}" class="btn btn-light btn-block text-primary font-weight-bold">CONTINUE SHOPPING</a>
                    </div>
                    <div class="col-md-3 mt-3">
                        <a href="{{ route('checkout.index') }}" class="btn btn-danger btn-block font-weight-bold">PROCEED TO CHECKOUT</a>
                    </div>
                </div>
            </div>
        </div>

        @if (count($brands) > 0)
            <div class="row">
                @foreach ($brands as $brand)
                    <div class="col-md-2 offset-md-1">
                        <img src="{{ asset('../storage/uploads/brands/'.$brand->brand_logo) }}" alt="" class="img-fluid">
                    </div>
                @endforeach
            </div>
        @endif

    </div>

    <script type="text/javascript">

        const inputElements = document.querySelectorAll('input[type=number]');
        const apiURL = "{{ route('update.cart') }}"

        for (let i = 0; i < inputElements.length; i++) {
            inputElements[i].addEventListener('change', handleChange);
        }

        function handleChange(event) {

            let rowId = event.target.classList[1]
            let quantity = event.target.value

            const data = {
                row_id: rowId,
                quantity: quantity
            }

            // Make API request
            axios.post(apiURL, data)
            .then(res => {
                console.log(res.data)

                let status = res.data

                if (status === 'Success') {
                    // setTimeout(() => {
                        location.reload()
                    // }, 500);
                }

            })
            .catch(err => {
                console.log('Error updating cart')
            })

        }

    </script>

@endsection
