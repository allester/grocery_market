@extends('layouts.app2')

@section('content')
<div class="jumbotron bg-primary rounded-0" style="margin-top:-30px;height:200px;" id="welcomeJumbo">
    <div class="row">
        <div class="col-md-6 text-light mt-5">
           <h1>{{ $market->market }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    {{-- <form method="GET" action="{{ route('search.products.storefront') }}">
                        <input class="form-control form-control-lg mr-sm-2" type="search" placeholder="Search" name="search" aria-label="Search" id="searchForm">
                    </form> --}}
                </div>
                {{-- <div class="col-md-2">
                    <button type="submit" class="btn btn-light btn-block btn-lg text-primary font-weight-bold"><i class="fas fa-search mr-3"></i></button>
                </div> --}}
            </div>
        </div>
    </div>
</div>
    <div class="container mb-5">
        <div class="row mt-5 mb-5">
            <div class="col-md-6 offset-md-3">
                {{-- <form method="GET" action="{{ route('search.products.storefront') }}">
                    <input class="form-control form-control-lg mr-sm-2" type="search" placeholder="Search" name="search" aria-label="Search" id="searchForm">
                </form> --}}
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-9">
                {{-- Display Product details here --}}
                <div class="card border-0 shadow p-3 mb-5 bg-white rounded mb-3 mt-3" style="max-width: 890px;">
                    <div class="row no-gutters">
                      <div class="col-md-4">
                        <img src="{{ $item->image_url }}" class="card-img card-img-dimension img-fluid" alt="...">
                      </div>
                      <div class="col-md-7 offset-md-1">
                        <div class="card-body">
                            <h5 class="card-title">{{ $item->product_name }}</h5>
                            {{-- <hr /> --}}
                            <a href="#" class="text-primary mt-3 mb-3">
                                <h5 class="card-text text-primary mb-4">UGX <span>{{ $item->price }}</span></h5>
                            </a>
                            <form method="POST" action="{{ route('cart.add') }}">
                                {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $item->obfuscator }}">
                            <input type="hidden" name="market" value={{ $market->market }}>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button class="btn btn-danger btn-lg">
                                            Add To Cart
                                        </button>
                                    </div>
                                    {{-- <div class="col-md-10">
                                    </div> --}}
                                </div>
                            </form>
                        </div>
                      </div>
                    </div>
                </div>{{-- end of card --}}
                <div class="row">
                    {{-- <h3>Product Description</h3>
                    <div>
                        {!! $item->description !!}
                    </div> --}}
                    <div class="col-md-12">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                              <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                  <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Details
                                  </button>
                                </h2>
                              </div>

                              <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    {!! $item->description !!}
                                </div>
                              </div>
                            </div>
                        </div>

                        {{-- Display other products --}}
                        @if (count($other_products) > 0)
                            <h3 class="mt-5 mb-5 text-warning">Suggested for you</h3>
                            <div class="row">
                                @foreach ($other_products as $other_product)
                                    @if ($other_product->id !== $item->id)
                                        <div class="col-md-4">
                                            <a href="{{ route('items.show',['item' => $other_product->obfuscator, 'market' => $market->obfuscator]) }}" class="no-text-decoration">
                                                <div class="card border-0 shadow mb-5 bg-white rounded item-card">
                                                    <img src="{{ $other_product->image_url }}" style="height: 140px;" class="card-img-top" alt="...">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{ $other_product->product_name }}</h5>
                                                        <a href="#" class="btn btn-primary mt-3">
                                                            <h5 class="card-text text-white">UGX <span>{{ $other_product->price }}</span></h5>
                                                        </a>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif

                    </div>
                </div>
            </div>
            <div class="col-md-3" id="sideBanner">
                <img src="https://res.cloudinary.com/dk8qdxlvl/image/upload/e_improve,w_300,h_600,c_thumb,g_auto/v1624911226/ja-ma--gOUx23DNks-unsplash-min_tpvd5w.jpg" alt="">
            </div>
        </div>
        @if (count($brands) > 0)
            <div class="row mb-5 pb-5">
                @foreach ($brands as $brand)
                    <div class="col-md-2 offset-md-1">
                        <img src="{{ asset('../storage/uploads/brands/'.$brand->brand_logo) }}" alt="" class="img-fluid">
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection
