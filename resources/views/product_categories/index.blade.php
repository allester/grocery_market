@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Product Categories</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <a href="{{ route('product_categories.create') }}" class="btn btn-dark btn-block">Create Category</a>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-10 offset-md-1">
        @if ($product_categories)
                <div class="row">
                    @foreach($product_categories as $product_category)
                        <div class="col-md-3">
                            <a href="{{ route('product_categories.show', $product_category->obfuscator) }}">
                                <div class="card">
                                    <div class="card-body">
                                        {{ $product_category->category }}
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <p>No categories to display</p>
            @endif
        </div>
    </div>

</div>

@endsection
