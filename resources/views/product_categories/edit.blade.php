@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Edit Product Category</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <form method="POST" action="{{ route('product_categories.update', $product_category->id) }}" enctype="multipart/form-data">
                @method('PUT')
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="Category">Product Category:</label>
                    <input type="text" name="product_category" class="form-control" value="{{ $product_category->category }}" required>
                </div>

                <div class="form-group">
                    <label for="categoryImage">Category Image:</label>
                    <div class="input-group mb-3">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="fileUpload" name="picture">
                            <label class="custom-file-label" for="fileUpload">Choose File</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-dark btn-block">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function(){

        var fileLabel = $('.custom-file-label');

        $('.custom-file-input').on('change', function(){
            var fileName = $(this).val();

            fileLabel.text(fileName);
        });

    });

</script>

@endsection
