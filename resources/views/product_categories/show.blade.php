@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Category Details</h3>
        </div>
    </div>

    <hr />

    {{-- id 	category 	category_image 	user_id 	edited_by 	created_at 	updated_at --}}

    <p class="alert alert-success hidden">

    </p>

    <p class="alert alert-success hidden">

    </p>

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-body">
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <p class="card-text text-dark font-weight-bold">Category:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $product_category->category }}</p>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <p class="card-text text-dark font-weight-bold">Category Image:</p>
                        </div>
                        <div class="col-md-6">
                            <img src="{{ asset('../storage/uploads/categories/'.$product_category->category_image) }}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <p class="card-text text-dark font-weight-bold">Icon:</p>
                        </div>
                        <div class="col-md-6">
                            {!! $product_category->icon !!}
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <p class="card-text text-dark font-weight-bold">Featured Category:</p>
                        </div>
                        <div class="col-md-6">
                            @if ($product_category->featured == TRUE)
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="featuredCheck" value="{{ $product_category->obfuscator }}" checked>
                                <label class="custom-control-label" for="featuredCheck"></label>
                              </div>
                            @else
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="featuredCheck" value="{{ $product_category->obfuscator }}">
                                <label class="custom-control-label" for="featuredCheck"></label>
                              </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-4">
                    <a href="{{ route('product_categories.edit', $product_category->obfuscator) }}" class="btn btn-secondary btn-block">Edit</a>
                </div>
                <div class="col-md-4">
                    <a href="{{ route('category.add_icon', ['category' => $product_category->obfuscator]) }}" class="btn btn-info btn-block">Add Icon</a>
                </div>
                <div class="col-md-4">
                    <form method="POST" action="{{ route('product_categories.destroy', $product_category->obfuscator) }}">
                        @method('DELETE')
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger btn-block">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // alert('working');
        let checkbox = $('#featuredCheck');
        let checkBoxValue = $('#featuredCheck').val();
        let messageSuccess = $('.alert-success');
        let messageError = $('.alert-danger');
        // let productCategory = ;

        checkbox.on('change', function(){
            // console.log(checkBoxValue);
            $.ajax({
                method:  'POST',
                url: '/change_featured_status',
                data: {product_category: checkBoxValue},
                dataType: 'JSON',
                success: function(data){
                    // console.log(data);
                    messageSuccess.html(data);
                },
                error: function(data){
                    // console.log(data);
                    messageError.html(data);
                }
            });

        });

        setTimeout(function() {
	        $(".alert").alert('close');
	    }, 3000);

    });

</script>

@endsection
