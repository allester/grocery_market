@extends('layouts.app2')

@section('content')
<div class="jumbotron bg-primary rounded-0" style="margin-top:-30px;height:350px;" id="welcomeJumbo">
    <div class="row mt-5">
        <div class="col-md-4 text-light mt-5">
           <h1>Chakula</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    {{-- <form method="GET" action="{{ route('search.products.storefront') }}">
                        <input class="form-control form-control-lg mr-sm-2" type="search" placeholder="Search" name="search" aria-label="Search" id="searchForm">
                    </form> --}}
                </div>
                {{-- <div class="col-md-2">
                    <button type="submit" class="btn btn-light btn-block btn-lg text-primary font-weight-bold"><i class="fas fa-search mr-3"></i></button>
                </div> --}}
            </div>
        </div>
    </div>
</div>
    <div class="container mb-5">
        <div class="row">
            <div class="col-md-12">
                <h3 class="mt-4 mb-4 text-center">Order from a market closest to you</h3>
                @if (isset($markets) && count($markets) > 0)
                    <div class="row">
                        @foreach ($markets as $market)
                        <div class="col-md-4 mt-3">
                            <a href="{{ route('market.show', $market->obfuscator) }}">
                                <div class="card" style="">
                                    <img src="{{ $market->image_url }}" class="card-img-top" alt="...">
                                    <div class="card-body">
                                    <h5 class="card-title">{{ $market->market }}</h5>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="col-md-2">

            </div>
        </div>
        <hr style="border border-warning">
        {{-- @if (isset($product_categories) && count($product_categories) > 0)
            <div class="row mt-5 mb-5" id="Shop">
                <div class="col-md-10">
                    <div class="row">
                        @foreach ($product_categories as $category)
                            <div class="col-md-3 mb-4">
                                <a class="font-weight-bold categories_link text-decoration-none" data-toggle="tooltip" data-placement="bottom" title="" href="{{ route('category.show', $category->obfuscator) }}" id="{{ $category->obfuscator }}">
                                    <div class="card border-0 shadow p-3 mb-5 bg-white rounded">
                                        <div class="card-body text-center">
                                            <p class="text-primary" style="font-size:50px;">{!! $category->icon !!}</p>
                                            <h5 class="font-weight-bold text-warning">{{ $category->category }}</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif --}}
        {{-- <div class="row mt-5 mb-5">
            <div class="col-md-8">
                @if ($products)
                    <div class="row" id="productArea">
                        @foreach ($products as $product)
                            <div class="col-md-4">
                                <a href="{{ route('items.show', $product->obfuscator) }}" class="no-text-decoration">
                                    <div class="card border-0 shadow mb-5 bg-white rounded item-card" style="">
                                        <img src="{{ $product->image_url }}" style="height: 140px;" class="card-img-top" alt="...">
                                        <div class="card-body">
                                          <h5 class="card-title">{{ $product->product_name }}</h5>
                                            <a href="#" class="btn btn-primary mt-3">
                                                <h5 class="card-text text-white">UGX <span>{{ $product->price }}</span></h5>
                                            </a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="col-md-4 mb-5" id="sideBanner">
                <img src="https://res.cloudinary.com/dk8qdxlvl/image/upload/e_improve,w_300,h_600,c_thumb,g_auto/v1624911226/ja-ma--gOUx23DNks-unsplash-min_tpvd5w.jpg" alt="">
            </div>
        </div> --}}
    </div>

    <script type="text/javascript">

        $(document).ready(function(){
            // alert('jquery working');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.categories_link').on('click', function(){
                var elementID = $(this).attr("id");
                // alert(elementID);
                var subCategories = $('#subCategories');
                subCategories.empty();

                $.ajax({
                    method: 'GET',
                    url: '/category_related_sub_categories',
                    data: {elemID: elementID},
                    dataType: 'JSON',
                    success: function(data){
                        // console.log(data);

                        data.forEach(element => {
                            console.log(element['sub_category']);

                            // List sub categories on webpage
                            subCategories.append('<a class="no-text-decoration pointer_cursor sub_category_link mt-3" id="'+ element['obfuscator'] +'"><li class="list-group-item list-group-item-action">'+ element['sub_category'] +'</li></a>');

                        });

                    },
                    error: function(data){
                        // console.log(data);
                        // console.log("Error retrieving data");
                        subCategories.html("<div class='mt-3'><p>Error retrieving data</p><p>Please try again.</p></div>");
                    }
                });
            });

            $(document).on('click', '.sub_category_link', function(event){
                event.preventDefault();
                // console.log('working well');

                var subCategoryID = $(this).attr("id");
                // console.log(subCategoryID);
                var productArea = $('#productArea');

                $.ajax({
                    method: 'GET',
                    url: '/sub_category_related_products',
                    data: {sub_category_id: subCategoryID},
                    dataType: 'JSON',
                    success: function(data){
                        // console.log(data);

                        data.forEach(element2 => {
                            // console.log(element2['product_name']);
                            // console.log(element2['product_image']);

                            var product_image = element2['product_image'];
                            var image_path = 'storage/uploads/products/'+product_image;
                            // console.log(image_path);
                            var productID = element2['obfuscator'];

                            var url = '{{ route("items.show", ":id") }}';
                            url = url.replace(':id', productID);
                            // console.log(url);


                            productArea.html('<div class="col-md-6"><a href="'+ url +'" class="no-text-decoration"><div class="card border-0 shadow p-3 mb-5 bg-white rounded item-card"><div class="card-body border-light"><img src="' + image_path + '" class="img-fluid" alt=""><p class="card-title mt-3">'+element2['product_name']+'</p><h5 class="card-text text-dark">UGX <span>'+element2['price']+'</span></h5></div></div></a></div>');
                        });
                    },
                    error: function(data){
                        // console.log(data);
                        productArea.html("<div class='mt-3'><p>Error retrieving data.</p><p>Please try again.</p></div>");
                    }
                });


            });
        });

    </script>

@endsection
