@extends('layouts.app3')

@section('content')
<div class="jumbotron bg-primary rounded-0" style="margin-top:-30px;height:250px;" id="welcomeJumbo">
    <div class="row mt-3">
        <div class="col-md-4 text-light">
           <h1>Chakula</h1>
           <h2 class="text-warning">My Profile</h2>
        </div>
    </div>
</div>
<div class="container">
    @if (Cart::count() > 0)
        <div class="card border-0 shadow p-3 mb-5 bg-white rounded mt-5">
            <div class="row">
                <div class="col-md-6">
                <p class="card-text">You have @if(Cart::count() > 1) {{ Cart::count() }} items @else {{ Cart::count() }} item @endif in your shopping cart</p>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('cart') }}" class="btn btn-light btn-block text-primary font-weight-bold">VIEW SHOPPING CART</a>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('checkout.index') }}" class="btn btn-danger btn-block font-weight-bold">PROCEED TO CHECKOUT</a>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-6">
            <div class="card border-0 shadow p-3 mb-3 bg-white rounded mt-5">
                <div class="card-body">
                    <div class="text-center">
                        <i class="fas fa-user fa-2x text-primary"></i>
                        <p class="card-text font-weight-bold mt-3">Customer Details</p>
                    </div>
                    <div class="mt-3">
                        <div class="row">
                            <div class="col-md-6">
                                <p class="font-weight-bold">First Name:</p>
                            </div>
                            <div class="col-md-6">
                                <p>{{ $customer->first_name }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="font-weight-bold">Last Name:</p>
                            </div>
                            <div class="col-md-6">
                                <p>{{ $customer->last_name }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="font-weight-bold">Username:</p>
                            </div>
                            <div class="col-md-6">
                                <p>{{ $customer->user_name }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="font-weight-bold">Email:</p>
                            </div>
                            <div class="col-md-6">
                                <p>{{ $customer->email }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="font-weight-bold">Phone Number:</p>
                            </div>
                            <div class="col-md-6">
                                @if ($customer->phone_number == 'No Number')
                                    <p>Please add your phone number</p>
                                @else
                                    <p>{{ $customer->phone_number }}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3 mb-3">
                <div class="col-md-12">
                    <a href="{{ route('customer.profile.edit',[ 'id' => $customer->obfuscator]) }}" class="btn btn-primary btn-block shadow" data-toggle="tooltip" data-placement="bottom" title="Edit Profile"><i class="fas fa-edit"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card border-0 shadow p-3 mb-5 bg-white rounded mt-5">
                <div class="card-body">
                    <div class="text-center">
                        <i class="fas fa-map-marker-alt fa-2x text-primary"></i>
                        <p class="card-text font-weight-bold mt-3">Billing Address</p>
                    </div>
                    <div class="mt-3">
                        <div class="row">
                            <div class="col-md-12">
                                @if ($customer_address)
                                    {{ $customer_address->address }}
                                @else
                                    <p class="mt-5">Please add your billing address</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if ($customer_has_address == FALSE)
                <a href="{{ route('customer_addresses.create') }}" class="btn btn-primary btn-block shadow" data-toggle="tooltip" data-placement="bottom" title="Add Billing Address"><i class="fas fa-plus-square"></i></a>
            @else
                <a href="{{ route('customer_addresses.edit',[ 'id' => $customer->obfuscator]) }}" class="btn btn-primary btn-block shadow" data-toggle="tooltip" data-placement="bottom" title="Edit Billing Address"><i class="fas fa-edit"></i></a>
            @endif
        </div>
    </div>
    @if (count($brands) > 0)
        <div class="row mt-5">
            @foreach ($brands as $brand)
                <div class="col-md-2 offset-md-1">
                    <img src="{{ asset('../storage/uploads/brands/'.$brand->brand_logo) }}" alt="" class="img-fluid">
                </div>
            @endforeach
        </div>
    @endif
</div>

<script type="text/javascript">

    $(function(){
        $('[data-toggle="tooltip"]').tooltip()
    })

</script>

@endsection
