@extends('layouts.app3')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 offset-md-1">
            <h4>Search Results</h4>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-8 offset-md-1">
            <div class="mb-1">
                <span class="text-secondary">Search term: </span><span class="text-danger">{{ $query }}</span><br/>
                <span class="text-secondary">{{ $result_count }} results</span>
            </div>

            @if (count($products) > 0)
                @foreach ($products as $search_result)
                    <br />
                    <a href="{{ route('items.show', $search_result->obfuscator) }}" class="mt-3 font_weight_400" style="font-size:18px;">{{ str_limit($search_result->product_name, 100) }}</a>
                    <span class="text-secondary">{!! str_limit($search_result->description, 100) !!}</span><br />
                @endforeach
            @else
                <p>No search results returned</p>
            @endif


        </div>
    </div>

</div>

@endsection
