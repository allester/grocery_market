@extends('layouts.app')


@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Add Icon</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <form method="POST" action="{{ route('category.store_icon') }}">

                {{ csrf_field() }}

                <input type="hidden" name="category" value="{{ $product_category->obfuscator }}">

                <div class="form-group">
                    <label for="categoryIcon">Icon</label>
                    <input type="text" name="category_icon" class="form-control" required>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-dark btn-block">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

@endsection
