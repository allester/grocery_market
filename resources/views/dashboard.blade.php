@extends('layouts.app')

@section('content')
    <div class="jumbotron bg-primary rounded-0" style="margin-top:-20px;height:200px;" id="welcomeJumbo">
        <div class="row">
            <div class="col-md-4 text-light mt-5">
            <h1>Chakula</h1>
            <h2 class="text-warning">Dashboard</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-2 offset-md-1">
                <div class="card text-center border-0 shadow mb-5 bg-secondary rounded">
                    <div class="card-body">
                        <h5 class="card-text font-weight-bold text-light">Categories</h5>
                        <hr />
                        <h3 class="card-text text-warning font-weight-bold">{{ $category_count }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card text-center border-0 shadow mb-5 bg-secondary rounded">
                    <div class="card-body">
                        <h5 class="card-text font-weight-bold text-light">Sub Categories</h5>
                        <hr />
                        <h3 class="card-text text-warning font-weight-bold">{{ $sub_category_count }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card text-center border-0 shadow mb-5 bg-secondary rounded">
                    <div class="card-body">
                        <h5 class="card-text font-weight-bold text-light">Products</h5>
                        <hr />
                        <h3 class="card-text text-warning font-weight-bold">{{ $product_count }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card text-center border-0 shadow mb-5 bg-secondary rounded">
                    <div class="card-body">
                        <h5 class="card-text font-weight-bold text-light">Brands</h5>
                        <hr />
                        <h3 class="card-text text-warning font-weight-bold">{{ $brand_count }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card text-center border-0 shadow mb-5 bg-secondary rounded">
                    <div class="card-body">
                        <h5 class="card-text font-weight-bold text-light">Orders</h5>
                        <hr />
                        <h3 class="card-text text-warning font-weight-bold">{{ $order_count }}</h3>
                    </div>
                </div>
            </div>
        </div>{{-- end of .row --}}
        <div class="row">
            <div class="col-md-2 offset-md-1">
                <div class="card text-center border-0 shadow mb-5 bg-secondary rounded">
                    <div class="card-body">
                        <h5 class="card-text font-weight-bold text-light">Pending Orders</h5>
                        <hr />
                        <h3 class="card-text text-warning font-weight-bold">{{ $pending_orders_count }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card text-center border-0 shadow mb-5 bg-secondary rounded">
                    <div class="card-body">
                        <h5 class="card-text font-weight-bold text-light">Processed Orders</h5>
                        <hr />
                        <h3 class="card-text text-warning font-weight-bold">{{ $processed_orders_count }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card text-center border-0 shadow mb-5 bg-secondary rounded">
                    <div class="card-body">
                        <h5 class="card-text font-weight-bold text-light">Dispatched Orders</h5>
                        <hr />
                        <h3 class="card-text text-warning font-weight-bold">{{ $dispatched_orders_count }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card text-center border-0 shadow mb-5 bg-secondary rounded">
                    <div class="card-body">
                        <h5 class="card-text font-weight-bold text-light">Completed Orders</h5>
                        <hr />
                        <h3 class="card-text text-warning font-weight-bold">{{ $completed_orders_count }}</h3>
                    </div>
                </div>
            </div>
        </div> {{-- end of row --}}
        {{-- <div class="row mb-2">
            <div class="col-md-3 offset-md-1">
                <a href="{{ route('orders.index') }}">
                    <div class="card border-0 shadow bg-primary text-light rounded">
                        <div class="card-body">
                            <h6>Orders</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 offset-md-1">
                <div class="card border-0 shadow bg-primary text-light rounded">
                    <div class="card-body">
                        <h6>Products</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-3 offset-md-1">
                <div class="card border-0 shadow bg-primary text-light rounded">
                    <div class="card-body">
                        <h6>Categories</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-md-3 offset-md-1">
                <div class="card border-0 shadow bg-primary text-light rounded">
                    <div class="card-body">
                        <h6>Sub Categories</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-3 offset-md-1">
                <div class="card border-0 shadow bg-primary text-light rounded">
                    <div class="card-body">
                        <h6>Brands</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-3 offset-md-1">
                <div class="card border-0 shadow bg-primary text-light rounded">
                    <div class="card-body">
                        <h6>Regions</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-md-3 offset-md-1">
                <div class="card border-0 shadow bg-primary text-light rounded">
                    <div class="card-body">
                        <h6>Areas</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-3 offset-md-1">
                <div class="card border-0 shadow bg-primary text-light rounded">
                    <div class="card-body">
                        <h6>Delivery Team</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-3 offset-md-1">
                <div class="card border-0 shadow bg-primary text-light rounded">
                    <div class="card-body">
                        <h6>Products</h6>
                    </div>
                </div>
            </div>
        </div> --}}

        @if (count($all_brands) > 0)
            <div class="row">
                @foreach ($all_brands as $brand)
                    <div class="col-md-2 offset-md-1">
                        <img src="{{ asset('../storage/uploads/brands/'.$brand->brand_logo) }}" alt="" class="img-fluid">
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection
