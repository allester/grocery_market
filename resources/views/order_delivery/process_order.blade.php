@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h3>Order No: <span class="text-primary font-weight-bold">{{ $order->obfuscator }}</span></h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <h4>Order Summary</h4>
            <hr />

            @foreach ($order->products as $product)
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                @if ($product->pivot->item_picked == TRUE)
                                    <p class="font-weight-bold text-left" style="text-decoration:line-through;color:red;">
                                        <span class="hidden_title font-weight-bold mr-2">Item:</span>
                                        {{ $product->product_name }}
                                    </p>
                                @else
                                    <p class="font-weight-bold text-left">
                                        <span class="hidden_title font-weight-bold mr-2">Item:</span>
                                        {{ $product->product_name }}
                                    </p>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <p class="">
                                    <span class="hidden_title font-weight-bold mr-2">Quantity:</span>
                                    {{ $product->pivot->quantity }}
                                </p>
                            </div>
                            <div class="col-md-2">
                                <p class="">
                                    <span class="hidden_title font-weight-bold mr-2">Market:</span>
                                    {{ $product->pivot->market }}
                                </p>
                            </div>
                            <div class="col-md-3">

                                <p class="">
                                    <span class="hidden_title font-weight-bold mr-2">Cost: </span>
                                    UGX {{ number_format($product->pivot->total_cost) }}
                                </p>
                            </div>
                            <div class="col-md-1">
                                @if ($product->pivot->item_picked == TRUE)
                                    <input type="checkbox" name="check_item" value="{{ $product->id }}" id="checkItem" checked />
                                @else
                                    <input type="checkbox" name="check_item" value="{{ $product->id }}" id="checkItem" />
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="row mt-3 mb-5">
                <div class="col-md-4">
                    <p class="text-primary font-weight-bold">Grand Total:</p>
                </div>
                <div class="col-md-4">

                </div>
                <div class="col-md-4">
                    <p class="font-weight-bold">UGX {{ number_format($order->order_total) }}</p>
                    <span class="text-secondary">Delivery fee included</span>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <h4>Process Order</h4>
            <br />
            <hr />
            <form method="POST" action="{{ route('process_order_delivery') }}">

                {{ csrf_field() }}

                <input type="hidden" name="order_id" value="{{ $order->obfuscator }}">

                <button type="submit" class="btn btn-danger btn-block">Submit</button>

            </form>
        </div>
    </div>
</div>

<script>

    const checkItem = document.querySelector('#checkItem');
    const check_uncheck_url = "{{route('check_uncheck_item')}}";

    checkItem.addEventListener('change', checkOrderedItem);

    function checkOrderedItem(event) {
        // alert(event.target.value);

        let item_id = event.target.value;
        let order_id = "{{$order->id}}";

        console.log(item_id);
        console.log(order_id);

        axios.post(check_uncheck_url, {
            itemId: item_id,
            orderId: order_id
        })
        .then((response) => {
            console.log(response);
            window.location.reload();
        })
        .catch((error) => {
            console.log(error);
            window.loaction.reload();
        })
    }



</script>

@endsection
