@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Sub Category Details</h3>
        </div>
    </div>

    <hr />

    {{-- id 	category 	category_image 	user_id 	edited_by 	created_at 	updated_at --}}

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-body">
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <p class="card-text text-dark font-weight-bold">Sub Category:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $sub_category->sub_category }}</p>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <p class="card-text text-dark font-weight-bold">Category:</p>
                        </div>
                        <div class="col-md-6">
                            <p class="card-text">{{ $category->category }}</p>
                        </div>
                    </div>
                    @if ($sub_category->sub_category_image !== 'nofile.png')
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <p class="card-text text-dark font-weight-bold">Sub Category Image:</p>
                            </div>
                            <div class="col-md-6">
                                <img src="{{ asset('../storage/uploads/categories/'.$sub_category->sub_category_image) }}" alt="" class="img-fluid">
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-6">
                    <a href="{{ route('sub_categories.edit', $sub_category->obfuscator) }}" class="btn btn-secondary btn-block">Edit</a>
                </div>
                <div class="col-md-6">
                    <form method="POST" action="{{ route('sub_categories.destroy', $sub_category->obfuscator) }}">
                        @method('DELETE')
                        {{csrf_field()}}
                        @if ($sub_category->available == TRUE)
                            <button type="submit" class="btn btn-danger btn-block">Disable</button>
                        @else
                            <button type="submit" class="btn btn-success btn-block">Enable</button>
                        @endif

                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
