@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Create Sub Category</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <form method="POST" action="{{ route('sub_categories.store') }}" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="Category">Category:</label>
                    <select name="category" class="custom-select">
                        <option selected disabled>Choose a Category</option>
                        @if ($product_categories)
                            @foreach ($product_categories as $category)
                                <option value="{{ $category->id }}">{{ $category->category }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="form-group">
                    <label for="SubCategory">Sub Category</label>
                    <input type="text" name="sub_category" class="form-control">
                </div>

                <div class="form-group">
                    <label for="categoryImage">Sub Category Image:</label>
                    <div class="input-group mb-3">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="fileUpload" name="picture">
                            <label class="custom-file-label" for="fileUpload">Choose File</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-dark btn-block">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function(){

        var fileLabel = $('.custom-file-label');

        $('.custom-file-input').on('change', function(){
            var fileName = $(this).val();

            fileLabel.text(fileName);
        });


    });

</script>

@endsection
