@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-md-3 text-center">
            <h3>Product Sub Categories</h3>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <a href="{{ route('sub_categories.create') }}" class="btn btn-dark btn-block">Create Sub Category</a>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-10 offset-md-1">
            @if ($sub_categories)
                <div class="row">
                    @foreach($sub_categories as $sub_category)
                        <div class="col-md-3 mt-3">
                            <a href="{{ route('sub_categories.show', $sub_category->obfuscator) }}">
                                <div class="card">
                                    <div class="card-body">
                                        {{ $sub_category->sub_category }}
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <p>No sub categories to display</p>
            @endif
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-4 offset-md-4 text-center">
            {{ $sub_categories->links() }}
        </div>
    </div>

</div>

@endsection
